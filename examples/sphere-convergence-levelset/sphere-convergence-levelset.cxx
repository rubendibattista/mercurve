/* sphere-convergence-levelset
 * Copyright © 2018 Ruben Di Battista
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing
 * official policies, either expressed or implied, of Ruben Di Battista.
 *
 * This example computes the error on the curvatures computation at different
 * resolutions for a sphere built using level-sets
 */

#include <limits>
#include <numeric>

#include "LevelSetSphere.h"
#include "Surface.h"
#include "Volume.h"

using namespace hgve;

using dbl = std::numeric_limits<double>;

static const double sphereRadius = 2;

int main(int argc, char *argv[]) {
  int maxNumPoints = 16;
  int numOfRes = 4;

  // Handle arguments. First argument after command is max number of points
  // second number is the number of resolutions steps
  if (argc > 1) {
    maxNumPoints = atoi(argv[1]);
    numOfRes = atoi(argv[2]);
  }
  std::vector<int> resolutions;
  int dR = std::round(maxNumPoints / numOfRes);

  // Allocate different resolutions
  int curRes = 0;
  for (int i = 0; i < numOfRes; i++) {
    curRes += dR;
    resolutions.push_back(curRes);
  }

  double correctG = 1 / sphereRadius / sphereRadius;
  double correctH = 1 / sphereRadius;

  static const SimpleVector sphereCenter(2.5, 2.5, 2.5);

  std::cout << "# Number of Points\tError(G)\tError(H)"
            << "\n";
  for (auto res : resolutions) {
    auto basename = std::string("sphere-levelset") + std::to_string(res);

    Volume domain(res, res, res, SimpleVector(5, 5, 5));
    LevelSetSphere sphere(sphereRadius, sphereCenter);
    domain.AddLevelSetObject(sphere);

    auto spacing = domain.GetSpacing();
    double dx = std::accumulate(spacing.begin(), spacing.end(), 0.0);
    dx /= spacing.size();

    auto surface = domain.Triangulate();
    surface->Clean(1E-4);

    surface->ComputeCurvatures();
    auto curvatures = surface->GetCurvArray();

    surface->Write(basename + std::string(".ascii"));

    // Accumulate errors over all the points (L1-norm)
    double errG = 0, errH = 0, errA = 0;
    auto nc = surface->GetNumberOfPoints();

    for (auto p : surface->IterateOver<Point>()) {
      double curG = p.OneRingProperties().G();
      double curH = p.OneRingProperties().H();
      double curA = p.OneRingProperties().A();

      errG += curA * std::abs(curG - correctG);
      errH += curA * std::abs(curH - correctH);
      errA += curA;
    }

    errG /= errA;
    errH /= errA;

    std::cout.precision(dbl::max_digits10);
    std::cout << nc << "\t" << errH << "\t" << errG << std::endl;

    // Test averaging
    surface->Average(2 * dx);
    surface->Write(basename + std::string("-average.ascii"));

    for (auto p : surface->IterateOver<Point>()) {
      double curG = p.OneRingProperties().G();
      double curH = p.OneRingProperties().H();
      double curA = p.OneRingProperties().A();

      errG += curA * std::abs(curG - correctG);
      errH += curA * std::abs(curH - correctH);
      errA += curA;
    }

    errG /= errA;
    errH /= errA;

    std::cout.precision(dbl::max_digits10);
    std::cout << nc << "\t" << errH << "\t" << errG << std::endl;
  }
}
