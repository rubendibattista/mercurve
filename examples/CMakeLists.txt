
include(ListSubDirs)
ListSubDirs(example_subdirs ${CMAKE_CURRENT_LIST_DIR})

# Append the PROJECT_BINARY_DIR to CMAKE_PREFIX_PATH in order to be able to 
# find the configuration files at build time
list(APPEND CMAKE_PREFIX_PATH ${PROJECT_BINARY_DIR})

# Add all examples targets and test them
foreach(subdir ${example_subdirs})
    # Process the subdir-local CMakeLists.txt
    add_subdirectory(${subdir})

    # In some examples we read some external `.vtp` or `.xmf` files. We need
    # to copy them in the build dir.
    file(GLOB VTP_FILES "${subdir}/*.vtp")
    file(COPY ${VTP_FILES} DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/${subdir}")

    add_test(
        NAME example_${subdir}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${subdir}
        COMMAND ${subdir} )
    #TODO: Install them
endforeach()
