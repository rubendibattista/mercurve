```
 __                  _           __
/\ \               /' \         /\ `\
\ \ \___      __  /\ ,/'  __  __\`\  \     __
 \ \  _ `\  /'_ `\\ \ \  /\ \/\ \`\`\ \  /'__`\
  \ \ \ \ \/\ \L\ \\ \ `\\ \ \_/ |`\/' \/\  __/
   \ \_\ \_\ \____ \\ `\__\ \___/  /\__/\ \____\
    \/_/\/_/\/___L\ \`\/_/ \/__/   \/_/  \/____/
              /\____/
              \_/__/
```
[![pipeline status](https://gitlab.com/rubendibattista/mercurve/badges/master/pipeline.svg)](https://gitlab.com/rubendibattista/mercurve/-/commits/master)
[![coverage report](https://gitlab.com/rubendibattista/mercurve/badges/master/coverage.svg)](https://gitlab.com/rubendibattista/mercurve/-/commits/master)


![](docs/imgs/droplets-collision-3x.gif)*A collision between two particles in 
a DNS simulation from [CORIA](http://coria.fr). The field displayed is the 
Gauss curvature.*

## A library to compute differential geometry properties on 3D objects

Mercur(**v**)e (or Hg(**v**)e) is a C++ library currently built on top of VTK
aimed at calculating differential geometry properties of 3D objects within the
framework of fluid dynamics. Use cases are categorization of 3D objects (i.e.
liquid droplets) in DNS of two-phase flows or turbulent structure in DNS of
single-phase flows.

You can access the documentation at http://docs.mercurve.rdb.is

## Building the Library

To build this library you need: 
* A fairly recent `cmake` (3.10+)
* A fairly recent `vtk` (should work with VTK 7+) with support for `xdmf`
  files enabled

You should be able to install them with your distribution package manager or
you can checkout [spack](https://spack.readthedocs.io/en/latest/) or also you
can use the [Docker image](https://hub.docker.com/repository/docker/rdb91/hgve)
we use for testing that already ships all the needed dependencies, in
particular VTK with the XDMF module enabled.

In order to build it just create a directory named `build` and do an
out-of-source build with cmake

```
mkdir build
cd build
cmake ..
```

CMake should be able to find the simple dependencies by itself if they're
installed in standard location. If it doesn't you can provide the path to the
third party libraries manually, to force the discovery if you installed VTK in
non-standard locations. 

---
**NOTE**
Remember to load the right modules if your system is equipped with some sort 
of environment modules manager

---

### Testing the application
In order to enable the testing of the library, you need to switch on the right
flag within cmake

```
cmake -DBUILD_TESTS:BOOL=ON ..
```

In order to run the tests, you will need
[`Catch2`](https://github.com/catchorg/Catch2). If `cmake` is not able to find
it in the `PATH`, it will download it for you automatically

### Building the documentation
To build the documentation that is hosted at http://docs.mercurve.rdb.is, you
will need `doxygen` and `python` with some packages. The `CMakeLists.txt` won't
download the dependencies for you (yet?), so you need to install with your
package manager at least `doxygen` and `python`. 

Then you can create a virtualenv and install the python packages that are 
needed to render the documentation doing 

```
pip install -r requirements.txt
```

The `requirements.txt` file is found in the `docs` directory.
