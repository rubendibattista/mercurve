/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>

#include <vtkMarchingCubes.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkXMLImageDataWriter.h>

#include "BlockMerger.h"
#include "Volume.h"

namespace hgve {

void Volume::InitImageData(Volume *volume, IdType nx, IdType ny, IdType nz,
                           double dx, double dy, double dz) {
  auto image = volume->m_imageData;

  image->SetDimensions(nx, ny, nz);
  image->AllocateScalars(VTK_DOUBLE, 1);
  image->SetSpacing(dx, dy, dz);
}

Volume::Volume(IdType nx, IdType ny, IdType nz, double dx, double dy, double dz)
    : m_imageData(vtkSmartPointer<vtkImageData>::New()) {
  this->InitImageData(this, nx, ny, nz, dx, dy, dz);
}

Volume::Volume(IdType nx, IdType ny, IdType nz, SimpleVector p)
    : m_imageData(vtkSmartPointer<vtkImageData>::New()) {
  SimpleVector e1(1, 0, 0);
  SimpleVector e2(0, 1, 0);
  SimpleVector e3(0, 0, 1);

  auto dx = std::abs(p.Dot(e1)) / nx;
  auto dy = std::abs(p.Dot(e2)) / ny;
  auto dz = std::abs(p.Dot(e3)) / nz;

  this->InitImageData(this, nx, ny, nz, dx, dy, dz);
}

Volume::Volume(ThreeArray<IdType> n, ThreeArray<double> d)
    : Volume(n[0], n[1], n[2], d[0], d[1], d[2]) {}

Volume::Volume(vtkSmartPointer<vtkImageData> grid) : m_imageData(grid) {}

void Volume::AddLevelSetObject(const LevelSetObject &object) {
  auto dims = m_imageData->GetDimensions();
  for (int i = 0; i < dims[0]; i++) {
    for (int j = 0; j < dims[1]; j++) {
      for (int k = 0; k < dims[2]; k++) {
        int ijk[3] = {i, j, k};
        auto pId = m_imageData->ComputePointId(ijk);
        auto coords = m_imageData->GetPoint(pId);
        m_imageData->SetScalarComponentFromDouble(
            i, j, k, 0, object.SDF(coords[0], coords[1], coords[2]));
      }
    }
  }
}

std::unique_ptr<Surface> Volume::Triangulate() const {
  vtkSmartPointer<vtkMarchingCubes> contour =
      vtkSmartPointer<vtkMarchingCubes>::New();
  contour->SetInputData(m_imageData);
  contour->Update();

  return std::unique_ptr<Surface>(new Surface(contour->GetOutput()));
}

ThreeArray<double> Volume::GetSpacing() const {
  double *spacing = m_imageData->GetSpacing();
  return {spacing[0], spacing[1], spacing[2]};
}

IdType Volume::GetNumberOfPoints() const {
  return m_imageData->GetNumberOfPoints();
}

void Volume::Write(std::string filename) {
  vtkSmartPointer<vtkXMLImageDataWriter> writer =
      vtkSmartPointer<vtkXMLImageDataWriter>::New();

  writer->SetFileName(filename.c_str());
  writer->SetInputData(m_imageData);
  writer->SetCompressorTypeToZLib();
  writer->Write();
}

}  // namespace hgve
