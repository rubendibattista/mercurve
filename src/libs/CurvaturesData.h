/* Mercur(ve)
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CURVATURESDATA_H
#define CURVATURESDATA_H

#include <array>
#include <string>
#include <unordered_map>

namespace hgve {

/** @brief A class that describes its fields in an array of strings
 * and stores the values of the geometrical parameters in an unordered
 * map
 */
struct CurvaturesData {
  using CurvNames = std::array<std::string, 6>;

  const static CurvNames m_names;
  std::unordered_map<std::string, double> m_fields;

  CurvaturesData();
  CurvaturesData(double G, double H, double C, double SI, double A, double Q);

  double &G() { return m_fields[m_names[0]]; }
  const double &G() const { return m_fields.at(m_names[0]); }

  double &H() { return m_fields[m_names[1]]; }
  const double &H() const { return m_fields.at(m_names[1]); }

  double &C() { return m_fields[m_names[2]]; }
  const double &C() const { return m_fields.at(m_names[2]); }

  double &SI() { return m_fields[m_names[3]]; }
  const double &SI() const { return m_fields.at(m_names[3]); }

  double &A() { return m_fields[m_names[4]]; }
  const double &A() const { return m_fields.at(m_names[4]); }

  double &operator[](const std::string key) { return m_fields[key]; }
  const double &at(const std::string key) const { return m_fields.at(key); }

  /* Summing another CurvaturesData */
  friend CurvaturesData operator+(const CurvaturesData &a,
                                  const CurvaturesData &b);
  CurvaturesData &operator+=(const CurvaturesData &other);

  /* Subtracting another CurvaturesData */
  friend CurvaturesData operator-(const CurvaturesData &a,
                                  const CurvaturesData &b);
  CurvaturesData &operator-=(const CurvaturesData &other);

  /* Multiplying a double */
  friend CurvaturesData operator*(const CurvaturesData &a, const double lambda);
  CurvaturesData &operator*=(const double lambda);

  /* Multiplying another CurvaturesData */
  friend CurvaturesData operator*(const CurvaturesData &a,
                                  const CurvaturesData &b);
  CurvaturesData &operator*=(const CurvaturesData &other);

  /* Dividing a double */
  friend CurvaturesData operator/(const CurvaturesData &a, const double lambda);
  CurvaturesData &operator/=(const double lambda);

  /* Dividing another CurvaturesData */
  friend CurvaturesData operator/(const CurvaturesData &a,
                                  const CurvaturesData &b);
  CurvaturesData &operator/=(const CurvaturesData &other);
};
}  // namespace hgve

#endif /* CURVATURESDATA_H */
