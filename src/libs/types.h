/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPES_H
#define TYPES_H

#include <array>
#include <unordered_set>
#include <utility>
#include <vector>

#include <vtkIdList.h>
#include <vtkType.h>

#include "ItemHash.h"

namespace hgve {
// Forward declaration just to define the types
class Point;
class Cell;

using IdType = vtkIdType;
using IdList = vtkIdList;

/**
 * @brief Templated set type for each "iter-item" of the polydata
 */
template <typename IterItem>
using Set = std::unordered_set<IterItem, ItemHash<IterItem>>;

/**
 * @brief Templated 2-element pair
 */
template <typename IterItem>
using Pair = std::pair<IterItem, IterItem>;

/**
 * @brief Templated 3-element array
 */
template <typename IterItem>
using ThreeArray = std::array<IterItem, 3>;

/** @brief Enum of type of axes that are used to wrap indices while
 * merging blocks of a DNS simulation
 */
enum Axes { x, y, z };

}  // namespace hgve

#endif /* TYPES_H */
