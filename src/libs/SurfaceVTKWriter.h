/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SURFACEVTKWRITER_H
#define SURFACEVTKWRITER_H

#include "SurfaceWriter.h"
#include "SurfaceWriterFactoryReg.h"

namespace hgve {

/** @brief This class writes a Surface object to a VTK `.vtp` file
 */
class SurfaceVTKWriter : public SurfaceWriter {
 public:
  SurfaceVTKWriter(std::string filename, Surface &surface)
      : SurfaceWriter(filename, surface){};
  void Write() const override;
};

namespace factory {
SurfaceWriterRegistration<SurfaceVTKWriter> _SurfaceVTKWriter(
    std::string(".vtp"));
}
}  // namespace hgve

#endif /* SURFACEVTKWRITER_H */
