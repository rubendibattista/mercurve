/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SimpleVector.h"
#include <cmath>

namespace hgve {

SimpleVector::SimpleVector(ThreeArray<double> p) {
  m_x = p[0];
  m_y = p[1];
  m_z = p[2];
}

double SimpleVector::Norm2() const { return m_x * m_x + m_y * m_y + m_z * m_z; }

double SimpleVector::Norm() const { return std::sqrt(this->Norm2()); }

double SimpleVector::Dot(const SimpleVector &b) const {
  return m_x * b.m_x + m_y * b.m_y + m_z * b.m_z;
}

SimpleVector SimpleVector::Multiply(const SimpleVector &b) const {
  return SimpleVector(m_x * b.m_x, m_y * b.m_y, m_z * b.m_z);
}

SimpleVector SimpleVector::Divide(const SimpleVector &b) const {
  return SimpleVector(m_x / b.m_x, m_y / b.m_y, m_z / b.m_z);
}

SimpleVector Min(const SimpleVector &a, const SimpleVector &b) {
  return SimpleVector(std::min(a.X(), b.X()), std::min(a.Y(), b.Y()),
                      std::min(a.Z(), b.Z()));
}

SimpleVector Max(const SimpleVector &a, const SimpleVector &b) {
  return SimpleVector(std::max(a.X(), b.X()), std::max(a.Y(), b.Y()),
                      std::max(a.Z(), b.Z()));
}

SimpleVector SimpleVector::Clamp(const SimpleVector &minVal,
                                 const SimpleVector &maxVal) const {
  return Min(Max(*this, minVal), maxVal);
}

SimpleVector SimpleVector::Abs() const {
  return SimpleVector(std::abs(m_x), std::abs(m_y), std::abs(m_z));
}

// :: Operators ::
SimpleVector operator+(const SimpleVector &a, const SimpleVector &b) {
  return SimpleVector(a) += b;
}

SimpleVector &SimpleVector::operator+=(const SimpleVector &b) {
  m_x += b.m_x;
  m_y += b.m_y;
  m_z += b.m_z;

  return *this;
}

SimpleVector operator-(const SimpleVector &a, const SimpleVector &b) {
  return SimpleVector(a) -= b;
}

SimpleVector &SimpleVector::operator-=(const SimpleVector &b) {
  m_x -= b.m_x;
  m_y -= b.m_y;
  m_z -= b.m_z;

  return *this;
}

bool SimpleVector::operator==(const SimpleVector &b) const {
  return this->X() == b.X() && this->Y() == b.Y() && this->Z() == b.Z();
}

bool SimpleVector::operator!=(const SimpleVector &b) const {
  return !(*this == b);
}

SimpleVector SimpleVector::operator-() const & {
  return SimpleVector(-m_x, -m_y, -m_z);
}

SimpleVector &SimpleVector::operator*=(const double lambda) {
  m_x *= lambda;
  m_y *= lambda;
  m_z *= lambda;

  return *this;
}

SimpleVector operator*(const double lambda, const SimpleVector &v) {
  return SimpleVector(v) *= lambda;
}

SimpleVector operator*(const SimpleVector &v, const double lambda) {
  return SimpleVector(v) *= lambda;
}

SimpleVector &SimpleVector::operator/=(const double lambda) {
  m_x /= lambda;
  m_y /= lambda;
  m_z /= lambda;

  return *this;
}

SimpleVector operator/(const double lambda, const SimpleVector &v) {
  return SimpleVector(v) /= lambda;
}

SimpleVector operator/(const SimpleVector &v, const double lambda) {
  return SimpleVector(v) /= lambda;
}

SimpleVector operator^(const SimpleVector &a, const SimpleVector &b) {
  return SimpleVector(a) ^ b;
}

SimpleVector SimpleVector::operator^(const SimpleVector &b) {
  double x, y, z;

  x = this->Y() * b.Z() - this->Z() * b.Y();
  y = this->Z() * b.X() - this->X() * b.Z();
  z = this->X() * b.Y() - this->Y() * b.X();

  return SimpleVector(x, y, z);
}

}  // namespace hgve
