/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cmath>

#include "LevelSetCylinder.h"

namespace hgve {

LevelSetCylinder::LevelSetCylinder(double R, double h, SimpleVector center)
    : m_R(R), m_h(h), m_C(center) {}

double LevelSetCylinder::SDF(double x, double y, double z) const {
  // Let's reposition with the center
  x = x - m_C.X();
  y = y - m_C.Y();
  z = z - m_C.Z();

  SimpleVector p1(x, y, 0);
  SimpleVector p2(x, 0, z);

  return std::max((p1 - p2).Norm() - m_R, std::abs(x) - m_h / 2);
}

std::unique_ptr<LevelSetObject> LevelSetCylinder::clone() const {
  auto clone = std::unique_ptr<LevelSetObject>(
      new LevelSetCylinder(this->m_R, this->m_h, this->m_C));

  return clone;
}

}  // namespace hgve
