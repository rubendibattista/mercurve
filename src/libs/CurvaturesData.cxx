#include "CurvaturesData.h"

namespace hgve {

const CurvaturesData::CurvNames CurvaturesData::m_names = {
    "Gauss_Curvature", "Mean_Curvature", "Curvedness",
    "Shape Index",     "Area",           "Quality"};

CurvaturesData::CurvaturesData()
    : m_fields{{m_names[0], 0}, {m_names[1], 0}, {m_names[2], 0},
               {m_names[3], 0}, {m_names[4], 0}, {m_names[5], 0}} {
  // Do nothing in the body of the constructor
}

CurvaturesData::CurvaturesData(double G, double H, double C, double SI,
                               double A, double Q)
    : m_fields{{m_names[0], G},  {m_names[1], H}, {m_names[2], C},
               {m_names[3], SI}, {m_names[4], A}, {m_names[5], Q}} {
  // Do nothing in the body of the constructor
}

CurvaturesData operator+(const CurvaturesData &a, const CurvaturesData &b) {
  return CurvaturesData(a) += b;
}

CurvaturesData &CurvaturesData::operator+=(const CurvaturesData &other) {
  for (const auto &field : m_names) {
    this->operator[](field) += other.at(field);
  }

  return *this;
}

CurvaturesData operator-(const CurvaturesData &a, const CurvaturesData &b) {
  return CurvaturesData(a) -= b;
}

CurvaturesData &CurvaturesData::operator-=(const CurvaturesData &other) {
  for (const auto &field : m_names) {
    this->operator[](field) -= other.at(field);
  }

  return *this;
}

CurvaturesData operator*(const CurvaturesData &a, const CurvaturesData &b) {
  return CurvaturesData(a) *= b;
}

CurvaturesData &CurvaturesData::operator*=(const CurvaturesData &other) {
  for (const auto &field : m_names) {
    this->operator[](field) *= other.at(field);
  }

  return *this;
}
CurvaturesData operator*(const CurvaturesData &a, const double lambda) {
  return CurvaturesData(a) *= lambda;
}

CurvaturesData &CurvaturesData::operator*=(const double lambda) {
  for (const auto &field : m_names) {
    this->operator[](field) *= lambda;
  }

  return *this;
}

CurvaturesData operator/(const CurvaturesData &a, const CurvaturesData &b) {
  return CurvaturesData(a) /= b;
}

CurvaturesData &CurvaturesData::operator/=(const CurvaturesData &other) {
  for (const auto &field : m_names) {
    this->operator[](field) /= other.at(field);
  }

  return *this;
}

CurvaturesData operator/(const CurvaturesData &a, const double lambda) {
  return CurvaturesData(a) /= lambda;
}

CurvaturesData &CurvaturesData::operator/=(const double lambda) {
  for (const auto &field : m_names) {
    this->operator[](field) /= lambda;
  }

  return *this;
}

}  // namespace hgve
