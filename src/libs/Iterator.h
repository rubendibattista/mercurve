/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ITERATOR_H
#define ITERATOR_H

#include <cstddef>
#include <iterator>

namespace hgve {
/**
 * @author RdB
 * @brief The IterItem iterator class for the Surface container
 *
 * This class implements the iterator class to iterate over IterItems in
 * a Surface class
 */
template <typename IterItem>
class Iterator {
 private:
  // Holding a reference to IterItem
  IterItem _iter;

 public:
  /** Constructor from a IterItem object */
  Iterator(IterItem iter) : _iter(iter){};

  // :: Operators :: //

  Iterator &operator++() {
    _iter++;
    return *this;
  }
  Iterator operator++(int) {
    Iterator previous(*this);
    this->operator++();
    return previous;
  }

  IterItem &operator*() { return _iter; }
  IterItem *operator->() { return &_iter; }

  bool operator!=(const Iterator &other) { return this->_iter != other._iter; }
  bool operator==(const Iterator &other) { return this->_iter == other._iter; }

  // :: Attributes :: //

  // Iterators traits
  using value_type = IterItem;
  using difference_type = std::ptrdiff_t;
  using pointer = IterItem *;
  using reference = IterItem &;
  using iterator_category = std::forward_iterator_tag;
};

/**
 * @author RdB
 * @brief A small handy struct to be used in range-based loop
 *
 * The struct is needed to be able to perform range-based for loops on the
 * templated Iterator
 *
 * e.g.
 * for ( Point p : polydata.range<Point>() ) {
 *     // polydata.range() returns a Range struct
 *     // Do stuff with p
 * }
 */
template <typename IterItem>
struct Range {
  Iterator<IterItem> begin_iter, end_iter;
  Iterator<IterItem> begin() { return begin_iter; }
  Iterator<IterItem> end() { return end_iter; }
};
}  // namespace hgve
#endif /* ITERATOR_H */
