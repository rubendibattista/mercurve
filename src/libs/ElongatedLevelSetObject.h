/* Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ELONGATEDLEVELSETOBJECT_H
#define ELONGATEDLEVELSETOBJECT_H

#include "LevelSetObject.h"
#include "SimpleVector.h"

namespace hgve {
/** @brief A class representing a LevelSetObject obtained by elongation of a
 * primitive
 */
class ElongatedLevelSetObject : public LevelSetObject {
 private:
  LevelSetObjectPtr m_obj; /** A pointer to the object to elongate */
  SimpleVector m_v; /**< The vector representing the elongation along the three
                       coordinate axes */

 public:
  ElongatedLevelSetObject(LevelSetObjectPtr obj, SimpleVector v);

  double SDF(double x, double y, double z) const override;
  std::unique_ptr<LevelSetObject> clone() const override;
};
}  // namespace hgve

#endif /* ELONGATEDLEVELSETOBJECT_H */
