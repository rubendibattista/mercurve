/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FACTORY_H
#define FACTORY_H

#include <memory>
#include <string>
#include <unordered_map>

namespace hgve {

/** @brief A self registering factory base class
 */
template <typename T, typename ConstructorFun>
class Factory {
 protected:
  // Constructors and stuff must be protected to implement a Singleton pattern
  // that is also inheritable https://stackoverflow.com/a/34519373/1334711
  Factory() noexcept = default;
  Factory(const Factory &) = delete;
  Factory &operator=(const Factory &) = delete;

  std::unordered_map<std::string, ConstructorFun *> m_register;

  virtual ~Factory() = default;

 public:
  /** @brief Returns the instance of the Factory */
  static T &GetInstance() noexcept(std::is_nothrow_constructible<T>::value) {
    static T instance;

    return instance;
  }

  void Register(const std::string name, ConstructorFun *constructorFun) {
    m_register.insert({name, constructorFun});
  }
};

}  // namespace hgve

#endif /* FACTORY_H */
