/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LevelSetSphere.h"
#include "SimpleVector.h"

namespace hgve {
LevelSetSphere::LevelSetSphere(double radius, SimpleVector center)
    : m_R(radius), m_C(center) {}

double LevelSetSphere::SDF(double x, double y, double z) const {
  // Let's reposition with the center
  x = x - m_C.X();
  y = y - m_C.Y();
  z = z - m_C.Z();

  SimpleVector X(x, y, z);
  return X.Norm2() - m_R * m_R;
}

std::unique_ptr<LevelSetObject> LevelSetSphere::clone() const {
  auto clone =
      std::unique_ptr<LevelSetObject>(new LevelSetSphere(this->m_R, this->m_C));

  return clone;
}
}  // namespace hgve
