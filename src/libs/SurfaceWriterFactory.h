/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SURFACEWRITERFACTORY_H
#define SURFACEWRITERFACTORY_H

#include "Factory.h"

namespace hgve {
class Surface;
class SurfaceWriter;

// The specialization of the constructor for a SurfaceWriter object
using SurfaceWriterConstructorFun =
    std::shared_ptr<SurfaceWriter>(std::string filename, Surface &surface);

/** @brief A factory to create SurfaceWriter objects.
 *
 * The register keys are the file extensions. That mean, for example, that the
 * constructor function of the SurfaceVTKWriter class is indexed in the register
 * with the extension `.vtp`
 */
class SurfaceWriterFactory
    : public Factory<SurfaceWriterFactory, SurfaceWriterConstructorFun> {
 public:
  void Write(std::string(filename), Surface &);
};
}  // namespace hgve

#endif /* SURFACEWRITERFACTORY_H */
