/* Mercur(ve)
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVELSETELLIPSOID_H
#define LEVELSETELLIPSOID_H

#include "LevelSetObject.h"

namespace hgve {

/** @brief An ellipsoid in \f$\mathbb{R}^3\f$ represented by its SDF
 *
 */
class LevelSetEllipsoid : public LevelSetObject {
 private:
  double m_Rx; /**< The x semi-axis of the ellipsoid */
  double m_Ry; /**< The y semi-axis of the ellipsoid */
  double m_Rz; /**< The z semi-axis of the ellipsoid */

 public:
  /** @brief Constructor
   *
   *  @param  Rx      x semi-axis
   *  @param  Ry      y semi-axis
   *  @param  Rz      z semi-axis
   */
  LevelSetEllipsoid(double Rx, double Ry, double Rz);

  double SDF(double x, double y, double z) const override;
  std::unique_ptr<LevelSetObject> clone() const override;
};
}  // namespace hgve

#endif /* LEVELSETELLIPSOID_H */
