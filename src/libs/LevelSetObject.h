/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVELSETOBJECT_H
#define LEVELSETOBJECT_H

#include <functional>
#include <memory>

#include "SimpleVector.h"
#include "types.h"

namespace hgve {

class CompositeLevelSetObject;
class ElongatedLevelSetObject;

/**
 * @brief An Abstract class representing a 3D surface object described by a
 * SDF in \f$\mathbb{R}^3\f$
 *
 * A 2D surface can be described by a level-set function, that means a
 * function:
 *
 * \f[
 *  \phi(\mathbf{x}) = 0
 * \f]
 *
 * In particular a Signed Distance Function (SDF) is a function that gives
 * the distance of any point of the domain \f$\mathbf{x}\f$ from the
 * surface itself. Being signed, that means we can also understand when we
 * are inside the object (+) and when we are outside (-).
 * Most of the operators and algorithms are inspired by:
 * https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 */
class LevelSetObject {
 public:
  /** @brief The distance function expression describing the object
   *
   * This function gives the value of the Signed Distance Function for
   * a specific point \f$\mathbf{x}$\f of the domain:
   *
   * \begin{equation}
   *  \phi: \phi(\mathbf{x})
   * \end{equation}
   */
  double SDF(ThreeArray<double> X) const { return this->SDF(X[0], X[1], X[2]); }

  /** @brief The distance function expression describing the object
   *
   * This function gives the value of the Signed Distance Function for
   * a specific point \f$\mathbf{x}\f$ of the domain:
   *
   * \begin{equation}
   *  \phi: \phi(\mathbf{x})
   * \end{equation}
   */
  double SDF(const SimpleVector &X) const {
    return this->SDF(X.X(), X.Y(), X.Z());
  }

  /** @brief The distance function expression describing the object
   *
   * This function gives the value of the Signed Distance Function for
   * a specific point (x, y, z) of the domain:
   *
   * \begin{equation}
   *  \phi: \phi(x, y, z)
   * \end{equation}
   */
  virtual double SDF(double x, double y, double z) const = 0;

  /** @brief An operation to generate an elongated version of the
   * LevelSetObject
   *
   * Elongate() splits a primitive in two (four or eight), moves the
   * pieces apart and connects them. See for reference:
   * https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
   */
  ElongatedLevelSetObject Elongate(const SimpleVector h) const;

  /** @brief Carving interiors of objects exploiting onioning
   * For carving interiors or giving thickness to primitives, without
   * performing expensive boolean operations (see below) and without
   * distorting the distance field into a bound, one can use "onioning".
   * You can use it multiple times to create concentric layers in your
   * SDF.
   * https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
   */
  // CompositeLevelSetObject Onion(double thickness) const;

  virtual std::unique_ptr<LevelSetObject> clone() const = 0;

  /** @brief Union of two CompositeLevelSetObjects */
  CompositeLevelSetObject operator+(LevelSetObject &other);

  /** @brief Subtraction of two CompositeLevelSetObjects */
  CompositeLevelSetObject operator-(LevelSetObject &other);

  /** @brief Intersection of two CompositeLevelSetObjects */
  CompositeLevelSetObject operator^(LevelSetObject &other);

  virtual ~LevelSetObject() = default;
};

using LevelSetObjectPtr = std::shared_ptr<LevelSetObject>;

}  // namespace hgve

#endif /* LEVELSETOBJECT_H */
