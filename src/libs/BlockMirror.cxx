#include <vtkImageReslice.h>
#include <vtkMatrix4x4.h>

#include "BlockMirror.h"
#include "types.h"

namespace hgve {

BlockMirror::BlockMirror(vtkSmartPointer<vtkImageData> imageData,
                         Direction axis, Orientation orientation)
    : m_block(imageData), m_axis(axis), m_orientation(orientation) {}

void BlockMirror::SetAxis(const Direction axis) { m_axis = axis; }

void BlockMirror::Mirror() {
  int axisAsInt = static_cast<int>(m_axis);

  // Copy the current block
  vtkSmartPointer<vtkImageData> flipImage =
      vtkSmartPointer<vtkImageData>::New();
  flipImage->DeepCopy(m_block);

  // :: Flipping ::

  // Flip and translate the copied block using an affine transformation
  // described by a 4x4 matrix
  vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
  matrix->Identity();

  // Set the column of the matrix related to the axis we're flipping against
  matrix->SetElement(axisAsInt, axisAsInt, -1.0);

  // :: Traslation ::

  // Retrieve the bounds of the original block
  double *bounds = m_block->GetBounds();

  // Set the origin of the flipped reference frame to twice the
  // length of the block
  double len = bounds[2 * axisAsInt + 1] - bounds[2 * axisAsInt];

  if (m_orientation == Orientation::POS)
    matrix->Element[axisAsInt][3] += 2 * len;

  // Use ImageReslice to apply the transform
  vtkSmartPointer<vtkImageReslice> reslicer =
      vtkSmartPointer<vtkImageReslice>::New();
  reslicer->SetInputData(flipImage);
  reslicer->SetResliceAxes(matrix);
  reslicer->SetOutputSpacing(flipImage->GetSpacing());
  reslicer->Update();

  m_block = reslicer->GetOutput();
}
}  // namespace hgve
