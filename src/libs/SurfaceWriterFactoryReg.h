/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SURFACEWRITERREGISTRATION_H
#define SURFACEWRITERREGISTRATION_H

#include "SurfaceWriterFactory.h"

namespace hgve {
namespace factory {
/** @brief This class is needed to be able to perform the registration of
 * classes within the SurfaceWriterFactory register
 */
template <typename WriterType>
class SurfaceWriterRegistration {
 public:
  SurfaceWriterRegistration(std::string name) {
    SurfaceWriterFactory::GetInstance().Register(
        name, [](std::string filename, Surface &surface) {
          return static_cast<std::shared_ptr<SurfaceWriter>>(
              std::make_shared<WriterType>(filename, surface));
        });
  }
};
}  // namespace factory
}  // namespace hgve

#endif /* SURFACEWRITERREGISTRATION_H */
