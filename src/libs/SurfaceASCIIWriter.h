/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SURFACEASCIIWRITER_H
#define SURFACEASCIIWRITER_H

#include "SurfaceWriter.h"
#include "SurfaceWriterFactoryReg.h"

namespace hgve {

/** @brief This class writes a Surface object to a simple ASCII file
 *
 * First all the properties of the points are listed
 * Then the connectivity of the cells is also saved
 *
 * @code
 * # Points section
 * x y z H G A ...
 *
 * # Cells section
 * Point1 Point2 Point3
 * @endcode
 */
class SurfaceASCIIWriter : public SurfaceWriter {
 private:
  const std::string POINT_HEADER = "# [Point Data]\n# X  Y  Z  ";
  const std::string CELL_HEADER = "# [Cell Data]\n# Point1  Point2  Point3\n";

  /** @brief This method takes care of serializing the surface when it also
   * contains geometrical data */
  void SerializeWithGeometry(std::ofstream &file) const;

  /** @brief This method takes care of serializing the surface when it does not
   * contain geometrical data
   */
  void SerializeWithoutGeometry(std::ofstream &file) const;

 public:
  SurfaceASCIIWriter(std::string filename, Surface &surface)
      : SurfaceWriter(filename, surface){};
  void Write() const override;
};

namespace factory {
SurfaceWriterRegistration<SurfaceASCIIWriter> _SurfaceASCIIWriter(
    std::string(".ascii"));
}
}  // namespace hgve

#endif /* SURFACEASCIIWRITER_H */
