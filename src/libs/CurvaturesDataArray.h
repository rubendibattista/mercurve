#ifndef CURVATURESDATAARRAY_H
#define CURVATURESDATAARRAY_H

#include <unordered_map>

#include "vtkDoubleArray.h"
#include "vtkSmartPointer.h"

#include "CurvaturesData.h"
#include "types.h"

namespace hgve {
/**
 * @brief A class storing `CurvatureData` elements
 */
class CurvaturesDataArray {
 private:
  using DataMap =
      std::unordered_map<std::string, vtkSmartPointer<vtkDoubleArray>>;

  DataMap m_data;
  IdType m_npoints;

 public:
  // Iterator traits
  using iterator = DataMap::iterator;
  using const_iterator = DataMap::const_iterator;

  CurvaturesDataArray();

  /** @brief Set the number of elements in the array */
  void SetNumberOfValues(IdType n);

  /** @brief Get the number of elements in the array */
  IdType GetNumberOfElements() const { return m_npoints; }

  /** @brief Append a `CurvaturesData` object to the array
   *  @param  i   The ID of the point for which to store the data
   *  @param  curvData    The actual CurvaturesData object to store
   */
  void Append(IdType i, CurvaturesData curvData);

  /** @brief Retrieve the CurvatureData structure at ID i */
  CurvaturesData Get(IdType i) const;

  /** @brief Write the array into a file
   */
  void Write(std::string filename) const;

  /* :: Iterator Stuff :: */
  iterator begin() { return m_data.begin(); }
  const_iterator begin() const { return m_data.begin(); }
  const_iterator cbegin() const { return m_data.cbegin(); }

  iterator end() { return m_data.end(); }
  const_iterator end() const { return m_data.end(); }
  const_iterator cend() const { return m_data.cend(); }
};

}  // namespace hgve
#endif /* CURVATURESDATAARRAY_H */
