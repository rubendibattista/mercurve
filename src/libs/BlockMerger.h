/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLOCKMERGER_H
#define BLOCKMERGER_H

#include <algorithm>
#include <vector>

#include <vtkImageData.h>
#include <vtkMultiBlockDataSet.h>

#include "GridBlock.h"
#include "types.h"

namespace hgve {

/** @brief A class responsible to merge an array of sorted blocks
 *  @author RdB
 */
class BlockMerger {
 private:
  /** @brief The pointer to the merged grid */
  vtkSmartPointer<vtkImageData> m_mergedImageData;

  /** @brief If the block are overlapping on the borders */
  bool m_overlapping = true;

  /** @brief Internal attribute to check if we need to cleanup blocks
   *
   * It is set to != 0 by the vtkMultiBlockDataSet version of the
   * constructor
   */
  IdType m_allocatedBlocks = 0;

  /** @brief The component index corresponding to the level-set
   *
   * For now it's the component 0. Check the @todo for improving this
   *
   * @todo We can use vtkPointData GetArray("Name of the Array") to
   * automatically select the right component by name.
   */
  int m_levelSetComponent = 0;

  /** @briefSorted vector of blocks to merge */
  std::vector<GridBlock> m_blocks;

  /** @brief Sort the internal blocks by coordinate
   *
   * Order of sorting: z - y - x
   */
  void SortBlocks();

  /** @brief Append block in the merged container next to another one
   * along a given axis */
  void InsertBlock(GridBlock block) const;

 public:
  /** @brief Constructor from a list of blocks
   *  @param[in] blocks    A vector of *sorted* GridBlock objects
   *
   * The blocks are not checked to be sorted
   */
  BlockMerger(std::vector<GridBlock> blocks);

  /** @brief Constructor from a vtkMultiBlockDataSet */
  BlockMerger(vtkSmartPointer<vtkMultiBlockDataSet> multiblock);

  /** @brief Set overlapping of boundaries
   *
   * If the overlapping of boundaries is set to true, each block is
   * sliced removing the first layer of points such that to not have same
   * data replicated
   */
  void SetBoundariesOverlapping(bool flag) { m_overlapping = flag; };

  /** @brief The actual method that merges the blocks
   *
   * The basic algorithm is that the blocks are looped and the number of
   * elements of each block is tracked down in order to allocate the
   * right memory for the underlying data structure for the merged grid.
   * When the x, y, z coordinate changes, you need to wrap the indices
   * since we start back from the starting i, j, k index of the previous
   * block.
   * Then memory is allocated and the blocks are actualy inserted in the
   * right position of the merged vtkImageData.
   */
  void Merge();

  /** @brief Returns the merged data **/
  vtkSmartPointer<vtkImageData> GetOutput() { return m_mergedImageData; }
};
}  // namespace hgve

#endif /* BLOCKMERGER_H */
