#include "SurfaceASCIIWriter.h"
#include "HGMath.h"

namespace hgve {
void SurfaceASCIIWriter::SerializeWithGeometry(std::ofstream &file) const {
  // :: Point Data ::
  // Headers
  file << POINT_HEADER;

  // Add headers for geometrical data
  auto curvArray = m_surface.GetCurvArray();
  for (auto kv : curvArray) {
    file << kv.first << std::setw(TAB_WIDTH) << " ";
  }
  file << "\n";
  // End Headers

  // Body
  for (IdType i = 0; i < m_surface.GetNumberOfPoints(); i++) {
    // Get point coordinates
    auto coords = m_surface.GetPoint(i);

    // Add Point coordinates to file
    for (auto coord : coords) {
      file << std::right << std::setw(PRECISION_WIDTH)
           << std::setprecision(maxPrecision) << std::scientific << coord
           << std::setw(TAB_WIDTH) << " ";
    }

    // Serialize also point data
    for (const auto &kv : curvArray) {
      file << kv.second->GetValue(i) << std::setw(TAB_WIDTH) << " ";
    }
    file << "\n";
  }

  // End Body

  // :: Cell Data ::
  file << CELL_HEADER;

  for (auto c : m_surface.IterateOver<Cell>()) {
    auto cellPoints = m_surface.GetCellPoints(c);
    for (auto p : cellPoints) {
      file << std::right << std::setw(ID_WIDTH) << p.Id()
           << std::setw(TAB_WIDTH) << " ";
    }
    file << "\n";
  }
}

void SurfaceASCIIWriter::SerializeWithoutGeometry(std::ofstream &file) const {
  // :: Point Data ::
  // Headers
  file << POINT_HEADER;
  file << "\n";
  // End Headers

  // Body
  for (IdType i = 0; i < m_surface.GetNumberOfPoints(); i++) {
    // Get point coordinates
    auto coords = m_surface.GetPoint(i);

    // Add Point coordinates to file
    for (auto coord : coords) {
      file << std::right << std::setw(PRECISION_WIDTH)
           << std::setprecision(maxPrecision) << std::scientific << coord
           << std::setw(TAB_WIDTH) << " ";
    }
    file << "\n";
  }
  // End Body

  // :: Cell Data ::
  file << CELL_HEADER;

  for (auto c : m_surface.IterateOver<Cell>()) {
    auto cellPoints = m_surface.GetCellPoints(c);
    for (auto p : cellPoints) {
      file << std::right << std::setw(ID_WIDTH) << p.Id()
           << std::setw(TAB_WIDTH) << " ";
    }
    file << "\n";
  }
}
void SurfaceASCIIWriter::Write() const {
  // Open file
  std::ofstream file;
  file.open(m_filename);

  if (m_surface.HasGeometricalData())
    this->SerializeWithGeometry(file);
  else
    this->SerializeWithoutGeometry(file);
  file.close();
}
}  // namespace hgve
