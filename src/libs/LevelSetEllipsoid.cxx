/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LevelSetEllipsoid.h"
#include "SimpleVector.h"

namespace hgve {
LevelSetEllipsoid::LevelSetEllipsoid(double Rx, double Ry, double Rz)
    : m_Rx(Rx), m_Ry(Ry), m_Rz(Rz) {}

double LevelSetEllipsoid::SDF(double x, double y, double z) const {
  SimpleVector X(x, y, z);
  SimpleVector R(m_Rx, m_Ry, m_Rz);

  double k0 = X.Divide(R).Norm();
  double k1 = X.Divide(R.Multiply(R)).Norm();
  return k0 * (k0 - 1.0) / k1;
}

std::unique_ptr<LevelSetObject> LevelSetEllipsoid::clone() const {
  auto clone = std::unique_ptr<LevelSetObject>(
      new LevelSetEllipsoid(this->m_Rx, this->m_Ry, this->m_Rz));

  return clone;
}
}  // namespace hgve
