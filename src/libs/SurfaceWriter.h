/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SURFACEWRITER_H
#define SURFACEWRITER_H

#include <string>

#include "Surface.h"

namespace hgve {
/** @brief The base class for Surface writers
 *
 * A derived class needs to override the Write() method to serialize the Surface
 * object to disk
 */
class SurfaceWriter {
 private:
  static Surface &RescaleSurface(Surface &surface);

 protected:
  std::string m_filename;
  Surface &m_surface;

 public:
  SurfaceWriter(std::string filename, Surface &surface);
  virtual void Write() const {};
  virtual ~SurfaceWriter() = default;
};
}  // namespace hgve

#endif /* SURFACEWRITER_H */
