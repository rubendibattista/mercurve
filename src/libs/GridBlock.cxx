/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GridBlock.h"
#include "HGMath.h"

namespace hgve {

GridBlock::GridBlock(vtkSmartPointer<vtkImageData> block) {
  int *dims = block->GetDimensions();
  m_nx = dims[0];
  m_ny = dims[1];
  m_nz = dims[2];

  double *bounds = block->GetBounds();
  m_x = bounds[0];
  m_y = bounds[2];
  m_z = bounds[4];

  m_block = block;
}

std::vector<int> GridBlock::GetDimensions() const { return {m_nx, m_ny, m_nz}; }

double *GridBlock::GetSpacing() const { return m_block->GetSpacing(); }

double *GridBlock::GetOrigin() const { return m_block->GetOrigin(); }

void GridBlock::SetStartIJK(int i, int j, int k) {
  m_hasStartIJK = true;
  m_startijk = {i, j, k};
}

std::vector<int> GridBlock::GetStartIJK() const {
  if (!m_hasStartIJK)
    throw std::runtime_error(
        "The starting i, j, k for this block "
        "have not been allocated.");

  return m_startijk;
}

void GridBlock::SetEndIJK(int i, int j, int k) {
  m_hasEndIJK = true;
  m_endijk = {i, j, k};
}

std::vector<int> GridBlock::GetEndIJK() const {
  if (!m_hasEndIJK)
    throw std::runtime_error(
        "The ending i, j, k for this block "
        "have not been allocated.");

  return m_endijk;
}

double GridBlock::GetScalarComponent(const int i, const int j, const int k,
                                     const int componentID) const {
  return m_block->GetScalarComponentAsDouble(i, j, k, componentID);
}

bool GridBlock::operator<(const GridBlock &other) const {
  double *spacing = m_block->GetSpacing();

  if (std::abs(m_x - other.m_x) / spacing[0] > SMALL)
    return m_x < other.m_x;
  else if (std::abs(m_y - other.m_y) / spacing[1] > SMALL)
    return m_y < other.m_y;
  return m_z < other.m_z;
}
}  // namespace hgve
