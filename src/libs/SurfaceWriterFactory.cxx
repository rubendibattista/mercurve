#include "SurfaceWriterFactory.h"
#include "SurfaceWriter.h"
#include "filesystem.h"

namespace hgve {
void SurfaceWriterFactory::Write(std::string filename, Surface &surface) {
  // Get filename extension
  auto filepath = fs::path(filename);
  auto ext = filepath.extension().string();

  // Convert extension to lowercase
  std::transform(ext.begin(), ext.end(), ext.begin(),
                 [](unsigned char c) { return std::tolower(c); });
  auto kv = m_register.find(ext);

  if (kv != m_register.end()) {
    // Writing using the required Writer
    kv->second(filename, surface)->Write();
  } else {
    throw std::runtime_error("Write format not recognized!");
  }
}
}  // namespace hgve
