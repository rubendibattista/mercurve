/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SURFACE_H
#define SURFACE_H

#include <vtkDoubleArray.h>
#include <vtkOctreePointLocator.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkTriangle.h>

#include "Cell.h"
#include "CurvaturesDataArray.h"
#include "Iterator.h"
#include "Point.h"
#include "types.h"

namespace hgve {
/**
 * @author RdB
 * @brief A class representing a 2D surface in a 3D space
 *
 * This class is basically a wrapper around the vtkPolyData data structure
 * and provides method to access the underlying data structure and its
 * iterables and also methods to compute curvatures
 */
class Surface {
  // I need SurfaceWriter to be able to access private members of this class
  // in order to manipulate the internal vtkPolyData object
  friend class SurfaceWriter;

 private:
  // :: Attributes :: //

  /** @brief A flag to check if curvatures have been computed */
  bool m_hasGeometricalData = false;

  /** @brief A pointer to the vtk object */
  vtkSmartPointer<vtkPolyData> m_polyPtr;

  /** @brief Point Locator */
  vtkSmartPointer<vtkOctreePointLocator> m_locator =
      vtkSmartPointer<vtkOctreePointLocator>::New();

  /** @brief Storing the curvatures data */
  CurvaturesDataArray m_curvData;

 public:
  /** @brief Constructor taking only a vtkSmartPointer<vtkPolyData> as argument.
   *
   */
  Surface(vtkSmartPointer<vtkPolyData> polydata);

  /**
   * @brief Copy-constructor
   */
  Surface(const Surface &other);

  /**
   * @brief Returs true if ComputeCurvatures has been called and the Surface
   * contains geometrical data
   */
  bool HasGeometricalData() const { return m_hasGeometricalData; }

  /**
   * @brief Read-only accessor to the CurvaturesDataArray
   */
  const CurvaturesDataArray &GetCurvArray() const { return m_curvData; }

  /**
   * @brief Read-only accessor to the underlying vtkPolyData
   */

  const vtkSmartPointer<vtkPolyData> GetPolyData() const { return m_polyPtr; }

  /**
   * @brief Cleanup degenerate cells in order to obtain a clean surface
   * triangulation
   * @param   rad The tolerance radius close points are merged within
   */
  void Clean(double rad);

  /**
   * @brief Remove degenerate cells with (e.g. two points cells).
   *
   * It's  generally needed when generating 3D objects directly and
   * not from a volumetric level-set to which a contouring procedure
   * is then applied
   */
  void Sanitize();

  /**
   * @brief Rebuild the surface connectivity
   */
  void Rebuild();

  /**
   * @brief Method to loop over objects in the polygonal data
   *
   * Use this method to loop over the objects composing the data
   * structure, for example Points, or Cells
   */
  template <typename IterItem>
  Range<IterItem> IterateOver() {
    Iterator<IterItem> begin_iterator(GetBeginItem<IterItem>());
    Iterator<IterItem> end_iterator(GetEndItem<IterItem>());

    return Range<IterItem>{begin_iterator, end_iterator};
  }

  /**
   * @brief Returns the first IterItem in the polygonal data
   */
  template <typename IterItem>
  IterItem GetBeginItem() {
    return {*this, 0};
  }

  /**
   * @brief Returns the last IterItem in the polygonal data
   */
  template <typename IterItem>
  IterItem GetEndItem() {
    return IterItem::GetEndItem(*this);
  }

  /**
   * @brief: Retrieve point coordinates from its ID
   * @return: A 3-element array `coords` containing the coordinates
   *  coords[0] = x
   *  coords[1] = y
   *  coords[2] = z
   */
  ThreeArray<double> GetPoint(IdType id) const;

  /**
   * @brief: Retrieve point ID from its coordinates
   * @return: The ID of the point
   */
  vtkIdType FindPoint(double x, double y, double z) const;

  /**
   * @brief Get total number of points
   * @return The total number of points
   */
  vtkIdType GetNumberOfPoints() const;

  /**
   * @brief Get total number of cells
   * @return The total number of points
   */
  vtkIdType GetNumberOfCells() const;

  /**
   * @brief Get Points composing a cell
   */
  ThreeArray<Point> GetCellPoints(const Cell);

  /**
   * @brief Returns an iterator over the given point's neighbour cells
   */
  Set<Cell> GetPointCells(const Point &point);

  /**
   * @brief Compute the area of the surface
   */
  double Area();

  /**
   * @brief Computes the values of the curvatures and stores them in the
   *
   */
  void ComputeCurvatures();

  /**
   * @brief Averages the point values with the value of the neighbours
   *
   * Given a specified radius, it averages the value for each single
   * point with the value of the points within the specified radius
   * averaged by the 1-ring area value of that point
   *
   * @todo: Factory pattern to be able to choose different algorithm
   */
  void Average(double radius);

  /**
   * @brief Writes the surface in a file that can be read by ParaView
   *
   * At the moment is XML VTK @todo: Factory pattern to be able to
   * choose it at runtime
   */
  void Write(std::string filename);

  // :: Operators ::
  Surface &operator=(const Surface &other);
};
}  // namespace hgve

#endif /* SURFACE_H */
