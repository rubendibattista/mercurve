/*
 * Mercur(v)e
 * Copyright © 2019 Ruben Di Battists
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NN_H
#define NN_H

#include <stdexcept>

namespace hgve {

/** @brief An exception to handle cases where we have non-manifold cells
 */
class NonManifoldException : public std::exception {
 public:
  const char *what() const noexcept;
};
}  // namespace hgve

#endif /* NONMANIFOLDEXCEPTION_H */
