#include "ElongatedLevelSetObject.h"

namespace hgve {

ElongatedLevelSetObject::ElongatedLevelSetObject(LevelSetObjectPtr obj,
                                                 SimpleVector v)
    : m_obj(obj), m_v(v) {}

double ElongatedLevelSetObject::SDF(double x, double y, double z) const {
  SimpleVector X(x, y, z);

  X = X - X.Clamp(-m_v, m_v);

  return m_obj->SDF(X);
}

std::unique_ptr<LevelSetObject> ElongatedLevelSetObject::clone() const {
  auto clone = std::unique_ptr<LevelSetObject>(
      new ElongatedLevelSetObject(this->m_obj, this->m_v));

  return clone;
}
}  // namespace hgve
