/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vtkMath.h>
#include <algorithm>

#include "Cell.h"
#include "Edge.h"
#include "HGMath.h"
#include "NonManifoldException.h"
#include "Point.h"
#include "SimpleVector.h"
#include "Surface.h"

static const double TWO_PI = 2 * vtkMath::Pi();

namespace hgve {

// :: Private ::
Point Point::GetEndItem(Surface &mesh) {
  return {mesh, mesh.GetNumberOfPoints()};
}

// :: Public ::
ThreeArray<double> Point::GetCoordinates() const {
  return m_mesh.GetPoint(m_id);
}

Set<Cell> Point::OneRingCells() const { return m_mesh.GetPointCells(*this); }

Set<Point> Point::OneRingPoints() const {
  Set<Point> ringPoints;
  for (Cell c : this->OneRingCells()) {
    for (Point p : c.Points()) {
      if (p.Id() != this->Id()) {
        // This is the "other" point
        ringPoints.insert(p);
      }
    }
  }

  return ringPoints;
}

double Point::Norm() const {
  SimpleVector sv(this->GetCoordinates());

  return sv.Norm();
}

Set<Edge> Point::OneRingEdges() const {
  // Get all points of the 1-ring star
  Set<Point> oneRingPoints = this->OneRingPoints();
  Set<Cell> oneRingCells = this->OneRingCells();

  if (oneRingCells.size() == 1) {
    throw NonManifoldException();
  }

  Set<Edge> oneRingEdges;

  // Loop over the 1-ring points
  for (Point p : oneRingPoints) {
    std::vector<Cell> EdgeCells;
    EdgeCells.reserve(2);  // Two cells share the same edge

    // Loop over the 1-ring cells to find the two cells that share
    // the edge
    bool firstFound = false;
    for (Cell c : oneRingCells) {
      ThreeArray<Point> cellPoints = c.Points();

      // If the point is found in the list of points composing the cell
      // that means the cell is one of the two sharing the current edge
      Point *foundPoint =
          std::find(std::begin(cellPoints), std::end(cellPoints), p);

      if (foundPoint != std::end(cellPoints)) {
        // We found one of the neighbour cells
        EdgeCells.push_back(Cell(m_mesh, c.Id()));

        // If we already found before another cell, this is the second
        // one so we can stop looping
        if (firstFound) {
          // Ensure that the cells are two
          assert(EdgeCells.size() == 2);

          auto cellEdgePair = std::make_pair(EdgeCells[0], EdgeCells[1]);

          oneRingEdges.insert(Edge(*this, p, cellEdgePair));
          break;
        } else {
          firstFound = true;
        }
      }
    }
  }

  // We find all the edges, let's return the set
  return oneRingEdges;
}

double Point::OneRingArea() const {
  double A = 0;  // Accumulate the 1-ring area

  // Loop over the 1-ring edges
  for (auto c : this->OneRingCells()) {
    // Compute the Mixed Area for the two neighbor cells
    A += c.MixedArea(*this);
  }

  return A;
}

CurvaturesData Point::OneRingProperties() const {
  double A = 0;                  // Accumulate the 1-ring area
  double theta = 0;              // Accumulate the angle defect
  double Q = 0;                  // Accumulate the mesh quality value
  SimpleVector HH(0, 0, 0);      // Accumulate the curvature flow
  SimpleVector normal(0, 0, 0);  // Accumulate the normal vector

  try {
    auto oneRingEdges = this->OneRingEdges();
  }
  // We're hitting non-manifold cells, we return
  // all zeros
  // FIXME: This is probably a hack that needs to be handled better
  catch (NonManifoldException &e) {
    return {0, 0, 0, 0, 0, 0};
  }

  // Loop over the 1-ring edges
  for (auto e : this->OneRingEdges()) {
    double tmpA = 0;

    // Cells that share the edge
    auto c1 = e.EdgeCells().first;
    auto c2 = e.EdgeCells().second;

    // Compute the Mixed Area for the two neighbor cells
    tmpA = c1.MixedArea(Point(this->m_mesh, this->Id()));
    normal += c1.Normal() * tmpA;
    A += tmpA;

    tmpA = c2.MixedArea(Point(this->m_mesh, this->Id()));
    normal += c2.Normal() * tmpA;
    A += tmpA;

    // Compute the cell quality for the two neighbor cells
    Q += c1.Quality();
    Q += c1.Quality();

    // Compute the Gauss-Curvature contribution for the cell pair
    theta += c1.GA(*this);
    theta += c2.GA(*this);

    // Compute the internal angles of the current edge
    double cotAlpha = c1.ComputeCotan(e);
    double cotBeta = c2.ComputeCotan(e);

    // Relative vector
    SimpleVector dp = e.Pc() - e.P2();

    // Sum edge contribution
    HH += (cotAlpha + cotBeta) * dp;
  }
  // Area-weighting of the normal
  normal /= A;

  // Looping over the cell pairs, we computed the values twice
  A /= 2;
  theta /= 2;
  Q /= 2;

  // This is the @f $\frac{1}{A_\text{mixed}}$ @f of @cite
  // meyer_discrete_2003
  HH /= 2 * A;

  // The scalar value of the curvature is half the magnitude of the flow
  double H = sgn(normal.Dot(HH)) * HH.Norm() / 2;
  double G = (TWO_PI - theta) / A;

  // Shape Index and Curvedness denominators
  double Delta = H * H - G;
  double Delta2 = 2 * H * H - G;

  // Init shape index and curvedness
  double SI = 0, C = 0;

  if (Delta > 0) SI = -4.0 / TWO_PI * std::atan(H / Delta);

  if (Delta2 > 0) C = std::sqrt(Delta2);

  return {
      G,   // Gauss Curvature
      H,   // Mean Curvature
      C,   // Curvedness
      SI,  // Shape-Index
      A,   // One-ring area
      Q    // Mesh Quality
  };
}

// :: Operators ::

SimpleVector Point::operator+(const Point &other) const {
  auto a = this->GetCoordinates();
  auto b = other.GetCoordinates();

  return SimpleVector(a) + SimpleVector(b);
}

SimpleVector Point::operator-(const Point &other) const {
  auto a = this->GetCoordinates();
  auto b = other.GetCoordinates();

  return SimpleVector(a) - SimpleVector(b);
}

SimpleVector Point::operator^(const Point &other) const {
  auto a = this->GetCoordinates();
  auto b = other.GetCoordinates();

  return SimpleVector(a) ^ SimpleVector(b);
}

Point &Point::operator=(const Point &other) {
  m_id = other.m_id;
  m_mesh = other.m_mesh;

  return *this;
}
}  // namespace hgve
