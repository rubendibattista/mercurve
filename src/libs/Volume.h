/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOLUME_H
#define VOLUME_H

#include <vtkDataObject.h>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>

#include "LevelSetObject.h"
#include "Surface.h"

namespace hgve {
/**
 * @brief A class representing a 3D volume on which a level-set function is
 * given
 *
 * This class is practically a wrapper over the vtkImageData class of VTK.
 * It's meant to provide an easier API over the results of the DNS. It also
 * ships method to handle multiblock files from DNS simulations
 */
class Volume {
 private:
  /** @brief The underlying vtkImageData */
  vtkSmartPointer<vtkImageData> m_imageData;

  /** @brief A satic method that fills up a vtkImageData with proper spacing and
   * details.  It is needed to be called  from the different flavours of
   * constructor
   *
   * @param   volume A pointer to an instance of Volume
   * @param   nx  Number of points in the x-direction
   * @param   ny  Number of points in the y-direction
   * @param   nz  Number of points in the z-direction
   * @param   dx  Spacing in the x-direction
   * @param   dy  Spacing in the y-direction
   * @param   dz  Spacing in the z-direction
   */
  static void InitImageData(Volume *volume, IdType nx, IdType ny, IdType nz,
                            double dx, double dy, double dz);

 public:
  /** @brief Constructor to internally generate the grid
   *
   * This constructor instantiate internally a vtkImageData object to act
   * as the Volume (instead of taking a vtkImageData (or Block) as
   * input).
   *
   * @param   nx  Number of points in the x-direction
   * @param   ny  Number of points in the y-direction
   * @param   nz  Number of points in the z-direction
   * @param   dx  Spacing in the x-direction
   * @param   dy  Spacing in the y-direction
   * @param   dz  Spacing in the z-direction
   */
  Volume(IdType nx, IdType ny, IdType nz, double dx, double dy, double dz);

  /** @brief Contructor to internally generate the grid
   *
   * This version of the constructor builds the grid giving the number of
   * points per direction and the coordinates of the upper-right point
   * of the grid (the bottom-left is assumed to be (0, 0, 0);
   *
   * @param   nx  Number of points in the x-direction
   * @param   ny  Number of points in the y-direction
   * @param   nz  Number of points in the z-direction
   * @param   p   The upper-right point
   */
  Volume(IdType nx, IdType ny, IdType nz, SimpleVector p);

  /** @brief Constructor to internally generate the grid
   *
   * This constructor instantiate internally a vtkImageData object to act
   * as the Volume (instead of taking a vtkImageData (or Block) as
   * input).
   *
   * @param   n  Array of the number of points in the three directions
   *              (x, y, z)
   * @param   d   Array of spacings in the three directions (x, y, z)
   */
  Volume(ThreeArray<IdType> n, ThreeArray<double> d);

  // @todo: We can probably implement a polymorphic Point.h class that can
  // represent whatever point in a dataset (i.e. a Point in Surface.h or
  // in Volume.h... Should be feasible... so we can implement the same
  // iterateOver<iterItem> method both for Volume and Surface (i.e. we
  // define a base class)

  /** @brief Constructor from pre-existing vtkImageData
   *
   * @param   grid    The level-set grid. It's a vtkImageData
   */
  Volume(vtkSmartPointer<vtkImageData> grid);

  /** @brief Materialize a LevelSetObject in the domain
   *
   * This method takes a LevelSetObject and writes it's Signed Distance
   * function
   *
   *  @param  object      The level set object to add in the domain
   */
  void AddLevelSetObject(const LevelSetObject &object);

  /** @brief This method triangulates the level-set returning a Surface
   */
  std::unique_ptr<Surface> Triangulate() const;

  /** @brief This method returns the points spacing in the three
   * dimensions
   */
  ThreeArray<double> GetSpacing() const;

  /** @brief This method returns the number of points contained
   * in the volume
   */
  IdType GetNumberOfPoints() const;

  /** @brief Writes the volume into a file that can be read by Paraview
   *  @param  filename    The name of the file
   *
   *  At the moment is XML VTK @todo: Factory pattern to be able to choose
   *  at runtime
   */
  void Write(std::string filename);

  /** @brief Set internal object
   */
  void SetInputData(vtkSmartPointer<vtkImageData> imageData) {
    m_imageData = imageData;
  };

  /** @brief Return pointer to internal object
   *
   */
  vtkSmartPointer<vtkImageData> GetOutput() { return m_imageData; };
};
}  // namespace hgve

#endif /* VOLUME_H */
