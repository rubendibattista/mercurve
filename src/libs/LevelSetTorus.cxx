/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>

#include "LevelSetTorus.h"

namespace hgve {

LevelSetTorus::LevelSetTorus(double R, double r, SimpleVector center)
    : m_R(R), m_r(r), m_C(center) {}

double LevelSetTorus::SDF(double x, double y, double z) const {
  /* The distance of the torus at point (x, y, z) is:
   *
   * \begin{equation}
   *  d(x, y, z) = \sqrt{
   *      (\sqrt{x^2 + z^2} - R)^2 + y^2
   *  } - r
   * \end{equation}
   *
   * I call the term $\sqrt{x^2 + z^2} - R$ q and I consider its squared
   * version.
   *
   * Then the term $\sqrt{q + y^2} - r$ can be tought as the norm of a 2D vector
   * that has components (q and y) (always considering the squared version)
   */

  // Let's reposition with the center
  x = x - m_C.X();
  y = y - m_C.Y();
  z = z - m_C.Z();

  SimpleVector p1(x, z, 0);  // Accessory variable
  double q = p1.Norm() - m_R;
  SimpleVector p2(q, y, 0);  // Accessory variable

  return p2.Norm() - m_r;
}

std::unique_ptr<LevelSetObject> LevelSetTorus::clone() const {
  auto clone = std::unique_ptr<LevelSetObject>(
      new LevelSetTorus(this->m_R, this->m_r, this->m_C));

  return clone;
}
}  // namespace hgve
