#include <vtkXMLPolyDataWriter.h>

#include "SurfaceVTKWriter.h"

namespace hgve {

void SurfaceVTKWriter::Write() const {
  vtkSmartPointer<vtkXMLPolyDataWriter> writer =
      vtkSmartPointer<vtkXMLPolyDataWriter>::New();

  writer->SetFileName(m_filename.c_str());
  writer->SetInputData(m_surface.GetPolyData());
  writer->SetCompressorTypeToZLib();
  writer->Write();
}
}  // namespace hgve
