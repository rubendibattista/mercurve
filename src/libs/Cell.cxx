/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vtkMath.h>
#include <unordered_map>

#include "Cell.h"
#include "Edge.h"
#include "HGMath.h"
#include "Point.h"
#include "SimpleVector.h"
#include "Surface.h"

namespace hgve {
// :: Private ::
Cell Cell::GetEndItem(Surface &mesh) { return {mesh, mesh.GetNumberOfCells()}; }

// :: Public ::
Cell::Cell(Surface &mesh, IdType cellId)
    : m_id(cellId),
      m_mesh(mesh),
      m_triangle(vtkSmartPointer<vtkTriangle>::New()) {}

ThreeArray<Point> Cell::Points() const {
  // @todo: Maybe we can cash the results since it's used in other methods
  return m_mesh.GetCellPoints(*this);
}

ThreeArray<SimpleVector> Cell::Edges() const {
  auto points = this->Points();

  return {points[1] - points[0], points[2] - points[0], points[2] - points[1]};
}

double Cell::Area() const {
  auto edges = this->Edges();

  return (edges[0] ^ edges[1]).Norm() / 2;
}

SimpleVector Cell::Normal() const {
  auto edges = this->Edges();
  auto normal = edges[0] ^ edges[1];
  return normal / normal.Norm();
}

double Cell::Perimeter() const {
  auto edges = this->Edges();

  return edges[0].Norm() + edges[1].Norm() + edges[2].Norm();
}

double Cell::MixedArea(const Point &pc) const {
  double A;  // Area

  // Points composing the cell
  auto cellPoints = this->Points();

  // Return the points of the cell are not the given point
  std::vector<Point> matches;
  for (auto p : cellPoints) {
    if (p != pc) matches.push_back(p);
  }

  assert(matches.size() == 2);

  // Generate the couple of points (i.e. the edges) to calculate the
  // angles between
  //        A-----B
  //         \   /
  //          \ /
  //           C
  //
  // def: AC = pA - pC
  SimpleVector AC = matches[0] - pc;
  SimpleVector BC = matches[1] - pc;
  SimpleVector AB = matches[0] - matches[1];

  bool isObtuse = false;  // This is set to true if any angle is > 90deg

  // cot(angle) at the center vertex of the 1-ring
  double cotC = this->ComputeCotan(AC, BC);
  // Check if it's obtuse
  if (cotC < 0) {
    isObtuse = true;
    A = this->Area() / 2;
  }

  // Other vertices
  double cotA = this->ComputeCotan(-AB, -AC);
  double cotB = this->ComputeCotan(AB, -BC);

  if ((cotA < 0) or (cotB < 0)) {
    // If isObtuse is true, we have two obtuse angles, smthg wrong!
    assert(isObtuse == false);

    isObtuse = true;
    A = this->Area() / 4;
  }

  if (!isObtuse) {
    // The area element is the Voronoi One
    A = 0.125 * (AC.Norm2() * cotB + BC.Norm2() * cotA);
  }

  assert(A != 0);

  return A;
}

double Cell::ComputeAngle(const SimpleVector &a, const SimpleVector &b) {
  return std::atan2((a ^ b).Norm(), a.Dot(b));
}

double Cell::ComputeAngle(const Edge &edge) const {
  auto cellPoints = this->Points();

  // Return the point of the cell that is not part of the edge
  auto p1 = std::find_if(cellPoints.begin(), cellPoints.end(),
                         [&edge](const Point &point) -> bool {
                           return (point != edge.Pc()) && (point != edge.P2());
                         });
  // Let's define the sides of the triangle to apply the cosine law
  SimpleVector a = edge.Pc() - *p1;
  SimpleVector b = edge.P2() - *p1;

  return this->ComputeAngle(a, b);
}

double Cell::ComputeCotan(const SimpleVector &a, const SimpleVector &b) {
  return a.Dot(b) / ((a ^ b).Norm());
}

double Cell::ComputeCotan(const Edge &edge) const {
  auto cellPoints = this->Points();

  // Return the point of the cell that is not part of the edge
  auto p1 = std::find_if(cellPoints.begin(), cellPoints.end(),
                         [&edge](const Point &point) -> bool {
                           return (point != edge.Pc()) && (point != edge.P2());
                         });
  // Let's define the sides of the triangle to compute the
  // cotangent between
  SimpleVector a = edge.Pc() - *p1;
  SimpleVector b = edge.P2() - *p1;

  return this->ComputeCotan(a, b);
}

double Cell::GA(const Point &pc) const {
  // Find the two points that are not the given one
  auto cellPoints = this->Points();
  std::vector<Point> edgePoints;
  edgePoints.reserve(2);

  std::copy_if(cellPoints.begin(), cellPoints.end(),
               std::back_inserter(edgePoints),
               [pc](const Point &point) -> bool { return (point != pc); });

  // Just to be sure that it's returning the right edges
  assert(edgePoints[0] != pc);
  assert(edgePoints[1] != pc);

  // Build relative vectors
  auto a = pc - edgePoints[0];
  auto b = pc - edgePoints[1];

  // Compute angle
  return this->ComputeAngle(a, b);
}

double Cell::Quality() const {
  double semiP = this->Perimeter() / 2;  // Semiperimeter
  double A = this->Area();

  // The ratio between inradius and circumradius is semiP*a*b*c/4/A**2
  // where a, b, c, are the length of the triangle edges
  auto edges = this->Edges();

  double abc = edges[0].Norm() * edges[1].Norm() * edges[2].Norm();

  return semiP * abc / A / A;
}
}  // namespace hgve
