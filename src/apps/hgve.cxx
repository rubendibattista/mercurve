/*
 * Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <docopt.h>
#include <iostream>

#include <vtkSmartPointer.h>
#include <vtkXdmf3Reader.h>

#include "MercurveConfig.h"

#include "ErrorObserver.h"
#include "RunHandler.h"
#include "filesystem.h"

static const std::string USAGE =
    R"(
Mercur(v)e (or Hg(v)e) is a program (and a library) to compute differential
geometry properties on 3D objects. In particular it gets as input the output of
a DNS simulation, i.e. a 3D Uniform grid with points storing level-set values,
then it triangulates the grid using a Marching Cube algorithm and computes the
differential geometry properties on the triangulated surface.

Usage:
    hgve [--mirror=<axis> ...] [options] <file>

Options:
    -D, --output_dir=<dir>      Directory where to store output files
                                [default: .]
    -h, --help                  Print this help message
    -a, --average=<timesDx>     Average points within the radius equal to
                                <timesDx> times the spacing of the original
                                grid (computed as the average of the three
                                axes)
    -s, --write_stats           Write point data to file
    --nooverlap                 Flag to set for a multiblock input file
                                in order to remove first layer of points that
                                are overlapping
    --merge-only                Execute the merge step only
    --mirror=<axis>             Before computing the curvature, mirror the
                                along the specified axis. Possible values of
                                <axis> are `+X`, `-X`, `+Y`, `-Y`, `+Z`, `-Z`,
                                the option to mirror along multiple axis (in
                                order).
    --sanitize                  True if you need to sanitize the resulting
                                triangulated surface. Check Surface::Sanitize
                                method for more information

Examples:
    Do the post-processing from a dataset that needs to be mirrored along
    the three axis writing down the statistics file and averaging with a
    radius of `2*dx`

    hgve --mirror=X --mirror=Y --mirror=Z -s -a 2 quarter-sphere.xmf


)";

using namespace hgve;

int main(int argc, char *argv[]) {
  // Use `docopt` to parse command line arguments defined in the
  // USAGE string
  docopt::Options args =
      docopt::docopt(LOGO + USAGE, {argv + 1, argv + argc}, true,
                     std::string("Mercur(v)e") + hgve::VERSION);

  // Print LICENSE
  std::cout << LOGO + LICENSE << std::endl << std::endl;

  auto filepath = fs::path(args["<file>"].asString());

  RunHandler handler(filepath);

  handler.Run(args);
}
