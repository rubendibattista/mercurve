#include "MultiBlockRunner.h"

namespace hgve {

MultiBlockRunner::MultiBlockRunner(vtkSmartPointer<vtkMultiBlockDataSet> grid)
    : ImageDataRunner(vtkSmartPointer<vtkImageData>::New()),
      m_merger(BlockMerger(grid)) {}

void MultiBlockRunner::Run(docopt::Options args) {
  // Parse arguments
  auto overlap = !(args["--nooverlap"].asBool());

  m_merger.SetBoundariesOverlapping(overlap);

  std::cout << "Merging the blocks in one\n"
            << "\n";

  m_merger.Merge();

  m_imageData = m_merger.GetOutput();

  ImageDataRunner::Run(args);
}
}  // namespace hgve
