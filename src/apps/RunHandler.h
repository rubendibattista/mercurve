#ifndef RUNHANDLER_H
#define RUNHANDLER_H

#include <unordered_map>

#include <docopt.h>
#include <vtkSmartPointer.h>
#include <vtkXdmf3Reader.h>
#include <vector>

#include "filesystem.h"

namespace hgve {

/** @brief A class that handles the different type of vtkDataObject that
 * can be returned by vtkXdmf3Reader and calls the right Runner class
 */
class RunHandler {
 private:
  vtkSmartPointer<vtkXdmf3Reader> m_reader;
  std::vector<double> m_timeSteps;

  /** @brief This runs the correct runner logic applied to a single time
   * step data object
   */
  void RunOneTimeStep(vtkSmartPointer<vtkDataObject> block,
                      docopt::Options args);

 public:
  RunHandler(fs::path file);

  /** @brief This runs the correct Runner based on the type of
   * m_dataObject
   *
   *  @param[in] args The command line arguments given when executing the
   *  main program
   */
  void Run(docopt::Options args);
};

}  // namespace hgve

#endif /* RUNHANDLER_H */
