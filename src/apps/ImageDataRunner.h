#ifndef IMAGEDATARUNNER_H
#define IMAGEDATARUNNER_H

#include "Runner.h"

#include <vtkImageData.h>

namespace hgve {

class ImageDataRunner : public Runner {
 protected:
  vtkSmartPointer<vtkImageData> m_imageData;

 public:
  ImageDataRunner(vtkSmartPointer<vtkImageData> imageData)
      : m_imageData(imageData) {}

  void Run(docopt::Options) override;

  ~ImageDataRunner() {};
};

}  // namespace hgve

#endif /* IMAGEDATARUNNER_H */
