# hg(v)e Stats
# Copyright © 2019 Ruben Di Battista
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of Ruben Di Battista.

""" The various reduce methods that are used to reduce the time series datasets
into reduced datasets (e.g. counting the number of objects for all the time
instants in a directory """

import os
import matplotlib.pyplot as plt
import natsort
import numpy as np


from .scene import Scene
from .utils import slugify


class ReduceMethod(Scene):
    """ A reduce method to be used at the end of processing all the time
    series instants

    Attributes
    ----------
    pipeline
        The :class:`Pipeline` object this reduce method is member of. You
        can operate on its global attributes (e.g. :attr:`~Pipeline.storage`)
    """

    pass


class GeneralPreprocess(ReduceMethod):
    """ Pre-process the data in the pipeline and make them available for the
    following :class:`ReduceMethod`
    """
    def _compute(self):
        data = {}
        files = []
        dtype = []

        # Process files to create the data structure
        for file, stats_dict in self.pipeline.storage.items():
            files.append(file)
            for field, value in stats_dict.items():
                data.setdefault(field, []).append(value)

        # Naturally sort by filename
        sort_indices = natsort.index_natsorted(files)

        for field, values in stats_dict.items():
            data[field] = np.array(data[field])[sort_indices]
            dtype.append((field, 'f8'))

        self.data = \
            np.vstack(list(data.values())).T.ravel().view(dtype)

    def _save(self):
        # Slugify field names
        col_names = [slugify(name) for name in self.data.dtype.names]
        np.savetxt(os.path.join(self.pipeline.work_dir, 'stats.stat'),
                   self.data, header=' '.join(col_names),
                   comments='# ')

    def process(self):
        self._compute()
        self._save()

        self.output_data = self.data


class ReducePlot(ReduceMethod):
    """ A class that represents a :class:`ReduceMethod` that plots data
    saving a figure for each present in :attr:`data`.
    """

    def _plot(self):
        for field in self.data.dtype.names:
            # Plot Data
            filename = slugify(field)
            plt.figure()
            plt.plot(self.data[field], '-o')
            plt.ylabel(field)
            plt.tight_layout()
            figure_path = os.path.join(self.pipeline.work_dir,
                                       f'{filename}.{self.pipeline.fmt}')
            plt.savefig(figure_path, format=self.pipeline.fmt, dpi=300)
            plt.close()

    def process(self):
        self._plot()

        self.output_data = self.data
