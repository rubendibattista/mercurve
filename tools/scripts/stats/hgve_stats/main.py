"""
stats.py
This script taks in input a folder containing `.dat` files produced by hg(v)e
and creates stats image and files from them

Usage:
    hgve-stats -l
    hgve-stats [options] <dir>

Options:
    -h, --help              Print this help message
    -R, --radius=<rad>      Normalizing radius [default: 1]
    -p, --pipeline=<p>      The name of the pipeline to run
                             [default: SpherePipeline]
    -l, --list              List the availables pipelines
    --format=<fmt>          Format of the output images. ('pdf', 'eps', 'png')
                             [default: png]




Mercur(v)e
Copyright © 2018 Ruben Di Battista

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import glob
import multiprocessing as mp

import matplotlib as mpl
mpl.use('Agg')
mpl.rcParams.update({
    'axes.labelsize': 'x-large',
    'axes.titlesize': 'x-large',
    'xtick.labelsize': 'large',
    'ytick.labelsize': 'large'
})



import matplotlib.pyplot as plt
import docopt

from .pipeline import pipelines

plt.set_cmap('viridis_r')


def main():
    args = docopt.docopt(__doc__)

    work_dir = args['<dir>']
    radius = float(args['--radius'])
    fmt = args['--format']
    p_name = args['--pipeline']
    list_p = args['--list']

    if list_p:
        for pipeline in pipelines:
            print(pipeline)
        os._exit(os.EX_OK)

    # List all the files in the current dir
    files = glob.glob(os.path.join(work_dir, '*.dat'))

    with mp.Manager() as manager:
        storage = manager.dict()
        # storage = dict()

        for file in files:
            storage[file] = manager.dict()

        # Init Pipeline
        pipeline_cls = pipelines[p_name]
        pipeline = pipeline_cls(work_dir=work_dir, storage=storage, fmt=fmt,
                                radius=radius)

        nproc = int(round(mp.cpu_count()))
        print(f'Running on {nproc} processes')

        with mp.Pool(nproc) as pool:
            pool.map(pipeline.consume, files)
        pipeline.end()
