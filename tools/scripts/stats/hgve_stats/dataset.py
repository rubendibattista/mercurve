import numpy as np
import numpy.lib.recfunctions as recfs


class Dataset:
    """ This is basically a n x m numpy array with named columns (and
    associated methods to retrieve some named column """

    def __init__(self, columns, data=None):
        # Use numpy structured array
        dtype = np.dtype([(col, 'f8') for col in columns])

        if data is not None:
            r, c = np.shape(data)
            self._data = np.empty(r, dtype=dtype)
            for i, col in enumerate(columns):
                self._data[col] = data[:, i]

    def __repr__(self):
        return repr(self._data)

    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def add_column(self, column_name, data):
        """ This method adds a column to the dataset """

        self._data = recfs.append_fields(self._data, column_name, data)

    @property
    def shape(self):
        return np.shape(self._data)
