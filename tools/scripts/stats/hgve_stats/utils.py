import re
import unicodedata


def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    # Source: https://stackoverflow.com/a/295466/1334711
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '',
                   value.decode('ascii', 'ignore')).strip().lower()
    value = re.sub(r'[-\s]+', '-', value)

    return value
