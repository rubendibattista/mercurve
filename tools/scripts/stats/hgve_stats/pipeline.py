# hg(v)e Stats
# Copyright © 2019 Ruben Di Battista
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of Ruben Di Battista.


"""
This module implements the post-processing pipeline for Mercur(v)e cases
output data. A base class that acts like an interface for the different output
scenes we want to extract and several specific pipeline entries for special
cases like the sphere or the ellipsoid where we know the exact analytical value
and so we can provide some additional insight comparing against the error.
"""
import abc
import os

import matplotlib.pyplot as plt
import numpy as np

from .scene import *
from .reduce import *

pipelines = {}


def register_pipeline(cls):
    pipelines[cls.__name__] = cls
    return cls


class Pipeline(abc.ABC):
    """ This is a series of scenes to be rendered and/or saved as output files.
    """

    def __init__(self, work_dir, scenes, reduce_methods, storage, fmt):
        self.storage = storage
        self.scenes = scenes
        self.reduce_methods = reduce_methods
        self.fmt = fmt
        self.work_dir = work_dir

    def consume(self, filename):
        self.filename = filename
        self.basename = os.path.splitext(self.filename)[0]

        # Loop over the scenes, assigning the previous one as dependency
        # to the next one
        prev = None
        for scene in self.scenes:
            scene.pipeline = self
            scene.dep = prev
            scene.process()
            prev = scene

    def end(self):
        """ This method runs at the end of the Pipeline """

        # Loop over the ReduceMethod, assigning the previous one as dependency
        # to the next one
        prev = None
        for method in self.reduce_methods:
            method.pipeline = self
            method.dep = prev
            method.process()
            prev = method


@register_pipeline
class GenericPipeline(Pipeline):
    def __init__(self, radius, *args, **kwargs):
        self.radius = radius
        self.H_def = 1/radius
        self.G_def = 1/radius/radius
        self.A_def = 4*np.pi*radius*radius

        scenes = [ReadDataScene(), SanitizeScene(), GaussBonnet(),
                  WilmoreEnergyScene(), FilterOutliersScene(), HGMapsScene()]

        reduce_methods = [GeneralPreprocess(), ReducePlot()]

        super().__init__(scenes=scenes, reduce_methods=reduce_methods, *args,
                         **kwargs)


@register_pipeline
class SpherePipeline(Pipeline):
    def __init__(self, radius, *args, **kwargs):
        self.radius = radius
        self.H_def = 1/radius
        self.G_def = 1/radius/radius

        scenes = [ReadDataScene(), SanitizeScene(), GaussBonnet(),
                  QualityScene(), ErrorMapsScene(error_fun=self.error_fun),
                  ErrorReproject(project_fun=self.project_fun),
                  WilmoreEnergyScene(), FilterOutliersScene(), HGMapsScene()]

        super().__init__(scenes=scenes, *args, **kwargs)

    @staticmethod
    def project_fun(data):
        G = data['Gauss Curvature']
        H = data['Mean Curvature']

        # Find outliers
        indices = np.where(H**2 - G < 0)

        # Project only the wrong values (keeping the same G)
        H[indices] = np.sqrt(G[indices])

        data['Mean Curvature'] = H

        return data

    @staticmethod
    def error_fun(data):
        G = data['Gauss Curvature']
        H = data['Mean Curvature']

        # Some points can be NaN or wrong
        with np.errstate(invalid='raise'):
            try:
                H_ref = np.sqrt(G)
            except FloatingPointError:
                # I return something small
                H_ref = 1e-10

        return (H - H_ref)/H_ref

    def end(self):
        """ This method runs at the end of the Pipeline """

        super().end()

        # Plot Outliers statistics
        points = []
        outliers = []
        H_error = []
        G_error = []
        H_error_p = []
        G_error_p = []
        G_Bonnet = []
        for file, stats_dict in self.storage.items():
            points.append(stats_dict['Number of Points'])
            outliers.append(stats_dict['Outliers Ratio'])
            H_error.append(stats_dict['H L1 Error'])
            G_error.append(stats_dict['G L1 Error'])
            H_error_p.append(stats_dict['G L1 Error (Proj)'])
            G_error_p.append(stats_dict['G L1 Error (Proj)'])
            G_Bonnet.append(stats_dict['Gauss Bonnet'])

        # Sort by number of points since entries are not sorted in the dict
        sort_indices = np.argsort(points)
        points = np.array(points)[sort_indices]
        outliers = np.array(outliers)[sort_indices]
        H_error = np.array(H_error)[sort_indices]
        G_error = np.array(G_error)[sort_indices]
        H_error_p = np.array(H_error_p)[sort_indices]
        G_error_p = np.array(G_error_p)[sort_indices]
        G_Bonnet = np.array(G_Bonnet)[sort_indices]

        stats = np.array([points, outliers, H_error, G_error,
                          H_error_p, G_error_p, G_Bonnet]).T

        # Save up data
        np.savetxt(os.path.join(self.work_dir, 'stats.stat'), stats,
                   header='Number_of_points Percentage_of_outliers '
                   'H_Error G_Error H_Error_Projected G_Error_Projected '
                   'Gauss_Bonnet',
                   comments='# ')

        # Plot Data
        plt.figure()
        plt.plot(points, outliers, '-o')
        plt.xlabel('Number of points')
        plt.ylabel('Percentage of outliers')
        plt.tight_layout()
        figure_path = os.path.join(self.work_dir, f'stats.{self.fmt}')
        plt.savefig(figure_path, format=self.fmt, dpi=300)
        # from matplotlib2tikz import save as tikz_save
        # tikz_save(os.path.join(work_dir, 'stats.tikz'),
        #           standalone_environment=True)
        plt.close()
