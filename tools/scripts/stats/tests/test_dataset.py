import pytest
import numpy as np

from hgve_stats.dataset import Dataset


@pytest.fixture
def _dataset():
    yield np.random.random((10, 3))


@pytest.fixture
def dataset(_dataset):
    yield Dataset(("A", "B", "C"), _dataset)


def test_setitem(dataset):
    r = dataset.shape[0]
    ones = np.ones(r)
    dataset['A'] = ones

    assert (dataset['A'] == ones).all()


def test_add_column(dataset):
    r = dataset.shape[0]
    rand_column = np.random.rand(r)
    dataset.add_column("Rand Column", rand_column)

    assert (dataset["Rand Column"] == rand_column).all()
