from setuptools import setup, find_packages

setup(
    name='hgve_stats',
    version='0.1',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    python_requires='>=3.5',
    install_requires=['numpy', 'matplotlib', 'natsort', 'docopt'],
    entry_points={
        'console_scripts': [
            'hgve-stats = hgve_stats:main'
        ],
    }
)
