#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
stats.py
This script taks in input a folder containing `.dat` files produced by hg(v)e
and creates stats image and files from them

Usage:
    stats.py [options] <dir>

Options:
    -h, --help              Print this help message
    --nbins=<nbins>         Number of bins [default: 100]
    -R, --radius=<rad>      Normalizing radius (e.g. the
                            radius of one spherical particle)[default: 1]
    --format=<fmt>          Format of the output images. ('pdf', 'eps', 'png')
                            [default: png]


Mercur(v)e
Copyright © 2018 Ruben Di Battista

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import glob

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import multiprocessing as mp
import numpy as np
import docopt
import re

plt.set_cmap('viridis_r')


def nonlinspace(xmin: float, xmax: float, numsteps: int, spacing: float = 2)\
        -> np.ndarray:
    """ This function generates a non-linearly-spaced array from xmin to xmax
    with more points in the interval [0-1].

    Args:
        xmin(float): Starting value of the array
        xmax(float): Ending value of the array
        numsteps(int): Number of array elements
        spacing(float): If > 1, points are accumulated near xmin, if < 0
            they accumulate near xmax. If = 1 evenly spaced.

    Returns:
        array(np.ndarray): The non-linearly-spaced-array
    """

    span = (xmax-xmin)
    dx = 1.0 / (numsteps-1)
    return [xmin + (i*dx)**spacing*span for i in range(numsteps)]


def H2_G(G):
    # Some points can be NaN or wrong
    with np.errstate(invalid='raise'):
        try:
            return np.sqrt(G)
        except FloatingPointError:
            # I return something small
            return 1e-10


def plot_histogram2d(x, y, bins, weights=None, nonlin=False):
    """ This function generates a 2D histogram and plots it as a contourf.

    nonlinspace(bool): If true it use a non linear contour colormap that has
        more levels close to zero

    """

    if weights is None:
        weights = np.ones(len(x))

    xy_bins, x_edges, y_edges = np.histogram2d(x, y, bins,
                                               weights=weights/np.sum(weights))

    x_centers = (x_edges[:-1] + x_edges[1:])/2
    y_centers = (y_edges[:-1] + y_edges[1:])/2

    if nonlin:
        levels = nonlinspace(np.min(xy_bins), np.max(xy_bins), 100)
    else:
        levels = 100

    plt.figure()
    cs = plt.contourf(x_centers, y_centers, xy_bins.T, levels=levels)
    plt.contour(cs, colors='#444444', linewidths=0.5, levels=cs.levels[::12])
    plt.colorbar(cs)
    plt.locator_params(axis='x', nbins=6)


def worker_function(filename, q, radius, fmt):
    """ This is the function doing the analysis in a single parallel process.
    It takes the filename as input and sends stuff to print in a queue that is
    then consumed by the writing process

    Args:
        filename(os.path): The path to the filename to open to do the analysis
        q(multiprocessing.Manager): A Manager queue to which "log" stuff are
            sent
        radius(float): The normalizing radius
        fmt(string): The format of the images

    """

    basename = os.path.splitext(filename)[0]

    # Logging
    print_log = []
    print_log.append(f'Processing {filename}')

    # Define normalizing values for H and G from `radius`
    H_def = 1/radius
    G_def = 1/radius/radius
    # A_def = 4*np.pi*radius*radius

    # Detect which columns hold Gauss and Mean Curvatures
    with open(filename, 'r') as f:
        first_line = f.readline()

    columns = re.split(r'\t+', first_line)

    for i, col in enumerate(columns):
        if 'Gauss Curvature' in col:
            iG = i
        elif 'Mean Curvature' in col:
            iH = i
        elif 'Area' in col:
            iA = i

    data = np.loadtxt(filename, skiprows=1)

    # Filter out NaNs
    indices = np.where(~np.isnan(data.T[iG, :]))
    for col in data.T[[iA, iH], :]:
        indices = np.intersect1d(indices, np.where(~np.isnan(col)))

    Gall = data[indices, iG]
    Hall = data[indices, iH]
    Aall = data[indices, iA]

    n = len(Aall)
    nbins = int(np.round(n**0.4))

    # Area distribution of the points
    Abins, Aedges = np.histogram(Aall, bins=nbins)
    Ac = (Aedges[:-1] + Aedges[1:])/2

    np.savetxt(f'{basename}-area.stat', np.transpose([Ac, Abins]),
               comments='#', header='Point_Area Number_of_points')

    plt.figure()
    plt.plot(Ac, Abins, '-o')
    plt.xlabel('Point Area')
    plt.ylabel('Number of points')
    plt.tight_layout()
    plt.savefig(f'{basename}-area.{fmt}', format=fmt, dpi=300)
    plt.close()

    # Error distribution for all the points

    # Sort for plotting
    rightH = H2_G(Gall)

    # Compute distances from the H^2 - G curve
    errH = (Hall - rightH)/rightH

    # 2D-PDF with Area
    plot_histogram2d(errH, Aall, nbins, weights=Aall, nonlin=True)
    plt.xlabel(r'$\frac{H - \sqrt{G}}{\sqrt{G}}$')
    plt.ylabel(r'$A$')
    plt.tight_layout()
    plt.savefig(f'{basename}-area-error-pdf.{fmt}', format=fmt, dpi=300)
    plt.close()

    # Compute np.histogram for normal error
    plt.figure()
    errHbins, errHedges = np.histogram(
        errH, bins=nbins, weights=Aall/np.sum(Aall))

    # Compute bin centers
    errHc = (errHedges[:-1] + errHedges[1:])/2
    np.savetxt(f'{basename}-errors.stat',
               np.transpose([errHc, errHbins]),
               header="Errors %-points")

    plt.plot(errHc, errHbins)
    plt.xlabel(r'$\frac{H - \sqrt{G}}{\sqrt{G}}$')
    plt.ylabel(r'Area-weighted point density')
    plt.tight_layout()
    plt.savefig(f'{basename}-errors.{fmt}', format=fmt, dpi=300)
    plt.close()

    # 2D-PDF without Area
    plot_histogram2d(errH, Aall, nbins, nonlin=True)
    plt.xlabel(r'$\frac{H - \sqrt{G}}{\sqrt{G}}$')
    plt.ylabel(r'$A$')
    plt.tight_layout()
    plt.savefig(f'{basename}-error-pdf.{fmt}', format=fmt, dpi=300)
    plt.close()

    # Curvature maps just for inliers
    inliers = np.where(Hall**2 > Gall)
    G = Gall[inliers]
    H = Hall[inliers]
    A = Aall[inliers]

    plt.figure()

    # Print integral values
    A_mean = np.sum(Aall)
    G_mean = np.sum(np.multiply(Gall, Aall))/4/np.pi

    print_log.append(f"Integral on the surface of H:{A_mean} and G:{G_mean}")

    # PDF
    HGAbins, HAedges, GAedges = np.histogram2d(
        H/H_def, G/G_def, bins=nbins, weights=A/np.sum(A),
        range=((0.8, 1.2), (0.8, 1.2)))

    HAc = (HAedges[:-1] + HAedges[1:])/2
    GAc = (GAedges[:-1] + GAedges[1:])/2

    plt.figure()
    cs = plt.contourf(HAc, GAc, HGAbins.T)
    plt.xlabel(r'$\frac{G}{G_{ref}}$')
    plt.ylabel(r'$\frac{H}{H_{ref}}$')
    plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
    plt.colorbar(cs)
    plt.tight_layout()

    plt.savefig(f'{basename}-area-weighted-pdf.{fmt}', format=fmt, dpi=300)
    plt.close()

    # PDF w/o area weights
    HGbins, Hedges, Gedges = np.histogram2d(
        H/H_def, G/G_def, bins=nbins, density=True,
        range=((0.8, 1.2), (0.8, 1.2)))

    Hc = (Hedges[:-1] + Hedges[1:])/2
    Gc = (Gedges[:-1] + Gedges[1:])/2

    plt.figure()
    cs = plt.contourf(Gc, Hc, HGbins.T, levels=100)
    plt.xlabel(r'$\frac{G}{G_{ref}}$')
    plt.ylabel(r'$\frac{H}{H_{ref}}$')
    plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
    plt.colorbar(cs)
    plt.tight_layout()

    plt.savefig(f'{basename}-pdf.{fmt}', format=fmt, dpi=300)
    plt.close()

    # Outliers statistics
    outliers = np.where(Hall**2 < Gall)
    num_out = len(outliers[0])

    if len(outliers[0]) > 0:
        outliers_ratio = num_out/len(Gall)

        print_log.append(f'{basename.split("/")[-1]}'
                         f'|Total Points:{len(Gall)}'
                         f'|Number of outliers: {num_out}'
                         f'|Optimal number of bins: {np.sqrt(num_out)}'
                         f'|Percentage of outliers: {outliers_ratio}')

        # Hout = Hall[outliers]
        # Gout = Gall[outliers]
        # Aout = Aall[outliers]

        # sort_indices = np.argsort(Gout)
        # Gout = Gout[sort_indices]
        # Hout = Hout[sort_indices]

        # Write log to file
        q.put('\n'.join(print_log) + '\n\n')


def log_writer(filename, q):
    """ This is the function runned by the writer process. It prints
    stuff in the output file """

    basename = os.path.splitext(filename)[0]
    stats_file = os.path.join(basename, 'stats.log')
    print(f'Logging Process: {os.getpid()}\n'
          f'Logging to file: {stats_file}')

    # Open the log file
    with open(stats_file, 'w') as f:
        while True:
            # Retrieve stuff from the queue
            m = q.get()

            # Handle the kill command
            if m == 'kill':
                f.write('killed')
                break

            f.write(f'{m} \n')
            f.flush()


if __name__ == "__main__":
    args = docopt.docopt(__doc__)

    work_dir = args['<dir>']
    radius = float(args['--radius'])
    fmt = args['--format']

    # List all the files in the current dir
    files = glob.glob(os.path.join(work_dir, '*.dat'))

    # Initialize the Manager Queue
    with mp.Manager() as manager:
        q = manager.Queue()
        nproc = int(round(mp.cpu_count()))
        print(f'Running on {nproc} processes')
        with mp.Pool(nproc) as pool:
            writer = pool.apply_async(log_writer, (work_dir, q))

            jobs = []
            for file in files:
                job = pool.apply_async(
                    worker_function, (file, q, radius, fmt))
                jobs.append(job)

            # Collect jobs
            for job in jobs:
                job.get()

            # Kill the writer
            q.put('kill')
            pool.close()
            pool.join()

    # Once we finished the parallel calculation let's generate a last
    # graph with outliers statistics per number of points
    with open(os.path.join(work_dir, 'stats.log'), 'r') as f:
        text = f.readlines()

        # Retrieve data from lines that match
        pattern_points = re.compile(r'Total Points:[ ]*([0-9]+)')
        pattern_outliers = re.compile(r'Percentage of outliers:[ ]*([0-9\.]+)')

        points = []
        outliers = []

        for line in text:
            points_match = re.search(pattern_points, line)
            outliers_match = re.search(pattern_outliers, line)
            if points_match:
                points.append(int(points_match.group(1)))
            if outliers_match:
                outliers.append(float(outliers_match.group(1)))

    sort_indices = np.argsort(points)

    points = np.array(points)[sort_indices]
    outliers = np.array(outliers)[sort_indices]
    outliers_data = np.array([points, outliers]).T

    np.savetxt(os.path.join(work_dir, 'stats.stat'), outliers_data,
               header='Number_of_points Percentage_of_outliers', comments='# ')

    plt.figure()
    plt.plot(points, outliers, '-o')
    plt.xlabel('Number of points')
    plt.ylabel('Percentage of outliers')
    plt.tight_layout()
    figure_path = os.path.join(work_dir, f'stats.{fmt}')
    plt.savefig(figure_path, format=fmt, dpi=300)
    # from matplotlib2tikz import save as tikz_save
    # tikz_save(os.path.join(work_dir, 'stats.tikz'),
    #           standalone_environment=True)
    plt.close()
