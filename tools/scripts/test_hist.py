import matplotlib as mpl
mpl.use('TkAgg')

import matplotlib.pyplot as plt
import numpy as np
from stats import histogram, histogram2d

# x = np.array([12, 3, 3, 2, 3, 1, 3, 4, 5, 6, 6, 2, 1, 2, 3])

x = np.array([1, 1, 1, 1, 1, 1, 1, 1, 5, 5, 5, 5, 5, 5, 5, 5])

pdf, bins = histogram(x, 10, weights=np.ones(np.size(x)))

# Compute centers
bc = (bins[:-1] + bins[1:])/2

assert(np.sum(np.multiply(pdf, np.diff(bins))) - 1 < 1e-6)

plt.plot(bc, pdf, 'o-')


pdf, bins, _ = histogram2d(x, x, 10)
pdf2, bins2, _ = np.histogram2d(x, x, 10, density=True)

bc = (bins[:-1] + bins[1:])/2

bc2 = (bins2[:-1] + bins2[1:])/2

plt.figure()
plt.contourf(bc, bc, pdf, levels=20)

plt.figure()
plt.contourf(bc2, bc2, pdf2, levels=20)

plt.show()
