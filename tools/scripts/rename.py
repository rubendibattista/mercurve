import os
import re
import sys

from fileinput import FileInput

CONSTANT_PREFIX=r'\w+\d+'
pattern = rf'({CONSTANT_PREFIX})_(\d{{5}})*_*st(\d{{6}})(_p)*(\d{{6}})*(.*\.\S+)'

print(f'Pattern: {pattern}')


FILE_REGEX = re.compile(pattern)
LINE_REGEX = re.compile(f'{pattern}:([\/\S]+)')



files = []
for (dirpath, dirnames, filenames) in os.walk('.'):
    files.extend(filenames)
    break

for file in files:
    matches = re.search(FILE_REGEX, file)
    try:
        prefix = matches.group(1)
        # first_index = int(matches.group(2))
        time = int(matches.group(3))
        # part = int(matches.group(5))
        suffix = matches.group(6)

    except AttributeError:
        print(f'{file} not processed')

    else:
        newfile = \
            f'{prefix}_st{time:06d}{suffix}'
        print(f'{file} => {newfile}')

        os.rename(file, newfile)

        if suffix == '.xmf':
            with FileInput(f'{newfile}', inplace=True) as f:
                for line in f:
                    try:
                        inner_matches = re.search(LINE_REGEX, line)
                        suffix = inner_matches.group(6)
                        namespace = inner_matches.group(7)
                    except AttributeError:
                        sys.stdout.write(line)
                        # sys.stderr.write(f'{re.escape(line)} not processed')
                        continue
                    else:
                        if suffix == '.h5':
                            inner_newfile = f'{prefix}_st{time:06d}{suffix}'
                            newline = re.sub(LINE_REGEX, f'{inner_newfile}:{namespace}', line)
                            # sys.stderr.write(f"Substituting {line} ==> {newline}")

                        sys.stdout.write(newline)


sys.exit(os.EX_OK)
