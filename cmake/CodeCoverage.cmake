# Coverage flags
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "(Apple)?[Cc]lang")
    message(FATAL_ERROR "Not Implemented! Yet. Use GCC for coverage")
elseif(CMAKE_COMPILER_IS_GNUCXX)

    find_program(GCOV_EXECUTABLE gcov)
    find_program(LCOV_EXECUTABLE lcov)

    if(NOT GCOV_EXECUTABLE)
        message(FATAL_ERROR "Gcov not found.")
    elseif(NOT LCOV_EXECUTABLE)
        message(FATAL_ERROR "Lcov not found.")
    endif()

    list(APPEND SUPPORTED_ADDITIONAL_FLAGS 
        "--coverage"
        )
    list(APPEND SUPPORTED_ADDITIONAL_LIBS "gcov")

    add_custom_target(coverage
        COMMAND ${LCOV_EXECUTABLE} --gcov-tool ${GCOV_EXECUTABLE}
            -c -d . -o "${PROJECT_NAME}.info"

        COMMAND ${LCOV_EXECUTABLE} --gcov-tool ${GCOV_EXECUTABLE} 
            -e "${PROJECT_NAME}.info" "\"*/src/*\"" -o "${PROJECT_NAME}.info"

        COMMAND ${LCOV_EXECUTABLE} --gcov-tool ${GCOV_EXECUTABLE}
            -l ${PROJECT_NAME}.info

        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    )

else()
    message(FATAL_ERROR "Coverage needs GCC or Clang")
endif()

