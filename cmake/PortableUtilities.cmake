# Mercur(v)e
# Copyright © 2020 Ruben Di Battista
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of Ruben Di Battista.

#[[ ==========================================================================

  This file provides tool that enable to create a portable bundle. It's
  inspired and it uses the functions provided by BundleUtilities but is more
  oriented to make portable tarballs instead of macOS app bundles.

=============================================================================]]


include(GNUInstallDirs)

# create_standalone_bundle(<target>
#     DESTINATION              <destination>
# )
# 
# This function takes a <target> as input, installs it in <bundle_destination>,
# retrieves its prerequisites (i.e. the libraries it depends on), and if they
# are not system libraries, it copies them). It also sets the RPATH accordingly.
function(create_standalone_bundle target)
    set(options)
    set(oneValueArgs DESTINATION)
    set(multiValueArgs)

    cmake_parse_arguments(STANDALONE_BUNDLE 
        "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
    )

    # Install the targets in the bundle destination
    install(TARGETS ${target}
        EXPORT  "${target}Targets"
        ARCHIVE DESTINATION 
            "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_LIBDIR}"
        INCLUDES DESTINATION 
            "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_INCLUDEDIR}"
        LIBRARY DESTINATION 
            "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_LIBDIR}"
        PUBLIC_HEADER DESTINATION 
            "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_INCLUDEDIR}"
        RUNTIME DESTINATION 
            "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_BINDIR}"
    )
    
    # Install the Targets file
    install(EXPORT "${target}Targets"
        FILE "${target}Targets.cmake"
        NAMESPACE "${target}::"
        DESTINATION "${STANDALONE_BUNDLE_DESTINATION}/${CMAKE_INSTALL_CMAKEDIR}"
    )
    
    # Configure bundle.cmake.in to use the right target name
    configure_file("${CMAKE_SOURCE_DIR}/cmake/bundle.cmake.in"
        "${CMAKE_BINARY_DIR}/bundle-${target}.cmake.in" @ONLY
    )

    # Evaluate generator expressions
    file(GENERATE
        OUTPUT "${CMAKE_BINARY_DIR}/bundle-${target}.cmake"
        INPUT "${CMAKE_BINARY_DIR}/bundle-${target}.cmake.in"
    )

    install(SCRIPT "${CMAKE_BINARY_DIR}/bundle-${target}.cmake")
endfunction()
