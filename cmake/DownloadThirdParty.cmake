# DownloadThirdParty
# Copyright © 2018 Ruben Di Battista
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.  2. Redistributions in
# binary form must reproduce the above copyright notice, this list of
# conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of Ruben Di Battista.

#######################################################################
#   This module provides functions that can be used to search for
#   a third party library and download it if it's not found
#######################################################################
include(FetchContent)


macro (find_or_download_package)
    set(options "")
    set(oneValueArgs NAME GIT_REPOSITORY GIT_TAG TARGET)
    set(multiValueArgs "")
    cmake_parse_arguments(TP_PACKAGE # for "third_party"
        "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    
    # Set GIT_TAG to master if not given
    if(NOT DEFINED TP_PACKAGE_GIT_TAG)
        set(TP_PACKAGE_GIT_TAG "master")
    endif()
    
    # Set the target name to the package name if not provided
    if(NOT DEFINED TP_PACKAGE_TARGET)
        set(TP_PACKAGE_TARGET ${TP_PACKAGE_NAME})
    endif()

    find_package(${TP_PACKAGE_NAME} QUIET)

    if(NOT ${TP_PACKAGE_NAME}_FOUND)
        message(STATUS "${TP_PACKAGE_NAME} could not be found on the system. It "
            "will be downloaded...")
        FetchContent_Declare(
            ${TP_PACKAGE_NAME}
            GIT_REPOSITORY ${TP_PACKAGE_GIT_REPOSITORY}
            GIT_TAG        ${TP_PACKAGE_GIT_TAG}
        )

        FetchContent_MakeAvailable(${TP_PACKAGE_TARGET})
    endif(NOT ${TP_PACKAGE_NAME}_FOUND)
endmacro(find_or_download_package) 


