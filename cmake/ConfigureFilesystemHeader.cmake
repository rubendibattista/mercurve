# Since on certain machines we can't include <filesystem> we check for
# it and we fallback to ghc if not found
include(CheckIncludeFileCXX)

if(NOT APPLE) # OR CMAKE_OSX_DEPLOYMENT_TARGET GREATER_EQUAL "10.15") when enabling C++17
    # First check standard filesystem works enabling C++17
    # check_include_file_cxx("filesystem" HAS_FILESYSTEM)
    # if(HAS_FILESYSTEM)
    #     set(FILESYSTEM_HEADER "<filesystem>")
    #     set(FILESYSTEM_NAMESPACE "std::filesystem")
    #     message(STATUS "Toolchains supports <filesystem>")
    #     return()
    # endif()

    # Second check experimental filesystem
    check_include_file_cxx("experimental/filesystem" HAS_EXPERIMENTAL_FILESYSTEM)
    if(HAS_EXPERIMENTAL_FILESYSTEM)
        message(STATUS "Toolchains supports <experimental/filesystem>")
        set(FILESYSTEM_HEADER "<experimental/filesystem>")
        set(FILESYSTEM_NAMESPACE "std::experimental::filesystem")
        set(FILESYSTEM_LIBRARY stdc++fs)
        return()
    endif()
endif()

if(APPLE)
    message(STATUS "macOs supports <filesystem> for macOS 10.15+ and C++17
    So we fallback to other filesystem implementations")
endif()

# Otherwise fallback to ghcFilesystem
message(STATUS "No support for native <filesystem>. Falling back to <ghc/filesystem>")
include(DownloadThirdParty)
find_or_download_package(
    NAME ghcFilesystem
    GIT_REPOSITORY https://github.com/gulrak/filesystem.git
)

set(FILESYSTEM_HEADER "<ghc/filesystem.hpp>")
set(FILESYSTEM_NAMESPACE "ghc::filesystem")
set(FILESYSTEM_LIBRARY ghcFilesystem::ghc_filesystem)

