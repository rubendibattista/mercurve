/** @brief Testing reading ARCHER output file
 */

#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <vtkXdmf3Reader.h>
#include <catch2/catch.hpp>

#include "Edge.h"
#include "Volume.h"
#include "test_config.h"

using namespace hgve;

SCENARIO("Post-processing of ARCHER files", "[archer]") {
  vtkSmartPointer<vtkXdmf3Reader> reader =
      vtkSmartPointer<vtkXdmf3Reader>::New();

  auto filename =
      TEST_SRC_DIR / "data" / "archer" / "NCCAS1_st000040_p000000.xmf";

  reader->SetFileName(filename.c_str());

  reader->Update();

  vtkSmartPointer<vtkImageData> image =
      vtkImageData::SafeDownCast(reader->GetOutputDataObject(0));

  Volume volume(image);

  auto surface = volume.Triangulate();
  surface->Write("lollo.vtp");

  WHEN("Calculating the area of each one ring") {
    double A = 0;
    for (auto p : surface->IterateOver<Point>()) {
      A += p.OneRingArea();
    }

    THEN(
        "The area is equal to the one computed using the normal "
        "triangles") {
      REQUIRE(A == Approx(surface->Area()));
    }
  }
}
