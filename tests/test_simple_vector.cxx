#include <catch2/catch.hpp>

#include "SimpleVector.h"

using namespace hgve;

TEST_CASE("Simple vector operations", "[vector]") {
  SECTION("Unit vectors") {
    SimpleVector e1(1, 0, 0);
    SimpleVector e2(0, 1, 0);
    SimpleVector e3(0, 0, 1);

    REQUIRE(e1 + e2 + e3 == SimpleVector(1, 1, 1));
    REQUIRE(e1 - e2 - e3 == SimpleVector(1, -1, -1));
    REQUIRE(e1.Dot(e2) == 0);
    REQUIRE(e2.Dot(e3) == 0);
    REQUIRE(e3.Dot(e1) == 0);
    REQUIRE(e1.Dot(e1) == Approx(e1.Norm() * e1.Norm()));
    REQUIRE((e1 ^ e2) == e3);
    REQUIRE((e2 ^ e3) == e1);
    REQUIRE((e3 ^ e1) == e2);
    REQUIRE((e3 ^ e2) == -e1);
    REQUIRE((e1 ^ e2) == e3);
  }

  SECTION("Angles computation") {
    SimpleVector a(1, std::sqrt(3), 0);
    SimpleVector b(1, 0, 0);

    REQUIRE(a.Dot(b) == Approx(1));
    REQUIRE(b.Dot(a) == Approx(1));

    double cosTheta = a.Dot(b) / (a.Norm() * b.Norm());

    REQUIRE(cosTheta == Approx(0.5));
  }

  SECTION("Element-wise computations") {
    SimpleVector a(1, 1, 1);
    SimpleVector b(2, 2, 2);

    REQUIRE(a.Multiply(b) == SimpleVector(2, 2, 2));
    REQUIRE(a.Divide(b) == SimpleVector(0.5, 0.5, 0.5));

    REQUIRE(a.Clamp(a, b) == a);
    REQUIRE(b.Clamp(a, b) == b);
    SimpleVector c(0, 1, 3);
    REQUIRE(c.Clamp(a, b) == SimpleVector(1, 1, 2));

    REQUIRE(Max(a, b) == b);
    REQUIRE(Min(a, b) == a);
    REQUIRE(Max(a, c) == SimpleVector(1, 1, 3));
  }
}
