#include <catch2/catch.hpp>

#include <vtkMath.h>

#include "CompositeLevelSetObject.h"
#include "LevelSetSphere.h"
#include "Point.h"
#include "Surface.h"
#include "Volume.h"

using namespace hgve;

SCENARIO("Testing Gauss-Bonnet theorem correct evaluation", "[gauss-bonnet]") {
  double baseRadius = GENERATE(0.01, 0.5);

  Volume domain(250, 250, 250,
                SimpleVector(baseRadius * 8, baseRadius * 8, baseRadius * 8));

  GIVEN("Three spheres in the domain that are not overlapping") {
    // Generate the central sphere
    double radius = baseRadius;
    SimpleVector center(baseRadius * 4, baseRadius * 4, baseRadius * 4);
    LevelSetSphere sphere(radius, center);

    // Generate the side spheres
    LevelSetSphere ear1(radius / 2, SimpleVector(baseRadius * 2, baseRadius * 2,
                                                 baseRadius * 4));
    LevelSetSphere ear2(radius / 2, SimpleVector(baseRadius * 6, baseRadius * 2,
                                                 baseRadius * 4));

    // Combine objects
    auto mickeyMouse = sphere + ear1 + ear2;

    // Materialize in the domain
    domain.AddLevelSetObject(mickeyMouse);

    // Triangulate the domain
    auto surface = domain.Triangulate();

    // Compute curvatures and save
    surface->ComputeCurvatures();

    THEN(
        "The value of the Gauss-Bonnet theorem gives correctly three "
        "objects") {
      double GA = 0;
      for (auto p : surface->IterateOver<Point>()) {
        auto curv = p.OneRingProperties();
        double G = curv.G();
        double A = curv.A();
        GA += G * A;
      }

      REQUIRE(GA / 4 / vtkMath::Pi() == Approx(3));
    }
  }

  GIVEN("Three spheres in the domain that overlapping") {
    // Generate the central sphere
    double radius = baseRadius * 2;
    SimpleVector center(baseRadius * 4, baseRadius * 4, baseRadius * 4);
    LevelSetSphere sphere(radius, center);

    // Generate the side spheres
    LevelSetSphere ear1(radius / 2, SimpleVector(baseRadius * 2, baseRadius * 2,
                                                 baseRadius * 4));
    LevelSetSphere ear2(radius / 2, SimpleVector(baseRadius * 6, baseRadius * 2,
                                                 baseRadius * 4));

    // Combine objects
    auto mickeyMouse = sphere + ear1 + ear2;

    // Materialize in the domain
    domain.AddLevelSetObject(mickeyMouse);

    // Triangulate the domain
    auto surface = domain.Triangulate();

    // Compute curvatures and save
    surface->ComputeCurvatures();

    THEN(
        "The value of the Gauss-Bonnet theorem gives correctly one "
        "object") {
      double GA = 0;
      for (auto p : surface->IterateOver<Point>()) {
        auto curv = p.OneRingProperties();
        double G = curv.G();
        double A = curv.A();
        GA += G * A;
      }

      REQUIRE(GA / 4 / vtkMath::Pi() == Approx(1));
    }
  }
}
