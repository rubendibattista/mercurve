/**
 * @brief Testing the LevelSet objects and operations
 */

#include <cmath>

#include <catch2/catch.hpp>

#include "test_config.h"

#include "CompositeLevelSetObject.h"
#include "ElongatedLevelSetObject.h"
#include "LevelSetCylinder.h"
#include "LevelSetSphere.h"
#include "LevelSetTorus.h"
#include "Surface.h"
#include "Volume.h"

using namespace hgve;

SCENARIO("2D surfaces indentified by level-set functions", "[levelset]") {
  GIVEN("A volume") {
    // A 100x100 square with dimension of (2, 2, 2);
    Volume domain(100, 100, 100, SimpleVector(2, 2, 2));

    WHEN("A LevelSetSphere is materialized in the domain") {
      static const double sphereRadius = 0.95;
      static const double sphereArea =
          4.0 * vtkMath::Pi() * sphereRadius * sphereRadius;

      // Positioning the sphere at the center of the domain
      static const SimpleVector sphereCenter{1, 1, 1};

      // Create a LevelSetSphere
      LevelSetSphere sphere(sphereRadius, sphereCenter);

      // Materialize the LevelSetSphere in the domain
      domain.AddLevelSetObject(sphere);

      THEN(
          "The surface of the sphere is equal to the analytical one "
          "within a tolerance") {
        auto sphere = domain.Triangulate();

        REQUIRE(sphere->Area() == Approx(sphereArea).margin(approxMargin));
      }
    }

    WHEN("A bigger LevelSetSphere is materialized in the domain") {
      static const double sphereRadius = 2;
      static const double sphereArea =
          4.0 * vtkMath::Pi() * sphereRadius * sphereRadius;

      // Bigger domain
      Volume bigDomain(100, 100, 100, SimpleVector(4.2, 4.2, 4.2));

      // Positioning the sphere at the center of the domain
      static const SimpleVector sphereCenter{2.1, 2.1, 2.1};

      // Create a LevelSetSphere
      LevelSetSphere sphere(sphereRadius, sphereCenter);

      // Materialize the LevelSetSphere in the domain
      bigDomain.AddLevelSetObject(sphere);

      THEN(
          "The surface of the sphere is equal to the analytical one"
          "within a tolerance") {
        auto sphere = bigDomain.Triangulate();
        sphere->ComputeCurvatures();

        REQUIRE(sphere->Area() == Approx(sphereArea).margin(approxMargin));
      }
    }

    WHEN("Two united LevelSetSpheres are materialized in the domain") {
      static const double sphereRadius = 0.445;
      static const double sphereArea =
          4.0 * vtkMath::Pi() * sphereRadius * sphereRadius;

      // Positioning the two spheres
      static const SimpleVector sphereCenter1(0.5, 0.5, 0.5);
      static const SimpleVector sphereCenter2(1.5, 1.5, 1.5);

      // Create two LevelSetSpheres
      LevelSetSphere sphere1(sphereRadius, sphereCenter1);
      LevelSetSphere sphere2(sphereRadius, sphereCenter2);

      // Combine the two spheres
      auto sphere = sphere1 + sphere2;

      // Materialize the LevelSetSphere in the domain
      domain.AddLevelSetObject(sphere);

      THEN(
          "The surface of the two spheres is equal to the analytical one"
          "within a tolerance") {
        auto sphere = domain.Triangulate();

        REQUIRE(sphere->Area() == Approx(2 * sphereArea).margin(approxMargin));
      }
    }

    WHEN("A LevelSetSphere is subtracted from another one") {
      static const double sphereRadius = 0.45;
      static const double sphereArea =
          4.0 * vtkMath::Pi() * sphereRadius * sphereRadius;

      // Positioning one sphere at the center
      static const SimpleVector sphereCenter1(1.0, 1.0, 1.0);

      // The other one on the center of a quadrant
      static const SimpleVector sphereCenter2(0.5, 0.5, 1.0);

      // Create two LevelSetSpheres
      LevelSetSphere sphere1(sphereRadius, sphereCenter1);
      LevelSetSphere sphere2(sphereRadius, sphereCenter2);

      // Subtract the two spheres
      auto sphere = sphere1 - sphere2;

      // Materialize the LevelSetSphere in the domain
      domain.AddLevelSetObject(sphere);

      THEN(
          "The surface of the subtraction is equal to one sphere"
          "within a tolerance") {
        auto sphere = domain.Triangulate();
        sphere->ComputeCurvatures();

        REQUIRE(sphere->Area() == Approx(sphereArea).margin(approxMargin));
      }
    }

    WHEN("A LevelSetSphere is intersected with another one") {
      static const double sphereRadius = 0.45;

      // Positioning one sphere at the center
      static const SimpleVector sphereCenter1(1.0, 1.0, 1.0);

      // The other one on the center of a quadrant
      static const SimpleVector sphereCenter2(0.5, 0.5, 1.0);

      // Distance between two centers
      auto d = (sphereCenter1 - sphereCenter2).Norm();

      // Intersection angle
      auto theta = std::acos(d / 2 / sphereRadius);

      // Spherical-cap Area
      auto area = 2 * vtkMath::Pi() * sphereRadius * sphereRadius *
                  (1 - std::cos(theta));

      // Create two LevelSetSpheres
      LevelSetSphere sphere1(sphereRadius, sphereCenter1);
      LevelSetSphere sphere2(sphereRadius, sphereCenter2);

      // Subtract the two spheres
      auto sphere = sphere1 ^ sphere2;

      // Materialize the LevelSetSphere in the domain
      domain.AddLevelSetObject(sphere);

      THEN(
          "The surface of the intersection is equal to the surface of "
          " the two spherical caps") {
        auto sphere = domain.Triangulate();
        sphere->ComputeCurvatures();

        REQUIRE(sphere->Area() == Approx(2 * area).margin(approxMargin));
      }
    }

    WHEN("A torus is materialized in the domain") {
      // Torus params
      static const double R = 0.5;
      static const double r = 0.25;
      static const double torusArea =
          2 * vtkMath::Pi() * R * 2 * vtkMath::Pi() * r;

      // Positioning the torus at the center of the domain
      static const SimpleVector torusCenter{1, 1, 1};

      // Create the torus
      LevelSetTorus torus(R, r, torusCenter);

      // Materialize the LevelSetTorus in the domain
      domain.AddLevelSetObject(torus);

      THEN(
          "The surface of the torus is equal to the analytical one"
          "within a tolerance") {
        auto surface = domain.Triangulate();

        REQUIRE(surface->Area() == Approx(torusArea).margin(approxMargin));
      }
    }

    WHEN("A cylinder is materialized in the domain") {
      // Torus params
      static const double R = 0.5;
      static const double h = 1.9;
      static const double cylinderArea =
          2 * vtkMath::Pi() * R * R + 2 * vtkMath::Pi() * R * h;

      // Positioning the cylinder at the center of the domain
      static const SimpleVector cylinderCenter{1, 1, 1};

      // Create the cylinder
      LevelSetCylinder cylinder(R, h, cylinderCenter);

      // Materialize the LevelSetCylinderin the domain
      domain.AddLevelSetObject(cylinder);

      THEN(
          "The surface of the cylinder is equal to the analytical one"
          "within a tolerance") {
        auto surface = domain.Triangulate();

        REQUIRE(surface->Area() ==
                Approx(cylinderArea)
                    // The convergence with the cylinder is slower
                    .margin(10 * approxMargin));
      }
    }

    WHEN("A capsule is materialized in the domain elongating a sphere") {
      static const double sphereRadius = 0.25;
      static const double sphereArea =
          4.0 * vtkMath::Pi() * sphereRadius * sphereRadius;

      // Positioning the sphere at the center of the domain
      static const SimpleVector sphereCenter{1, 1, 1};

      // Create a LevelSetSphere
      LevelSetSphere sphere(sphereRadius, sphereCenter);

      // Let's elongate the sphere
      auto capsule = sphere.Elongate(SimpleVector(0, 0, 0.05));

      // Materialize the LevelSetSphere in the domain
      domain.AddLevelSetObject(capsule);

      THEN(
          "The surface of the sphere is equal to the analytical one"
          "within a tolerance") {
        auto sphere = domain.Triangulate();

        REQUIRE(sphere->Area() == Approx(sphereArea).margin(approxMargin));
      }
    }
  }
}
