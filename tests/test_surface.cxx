/**
 * @brief Testing Surface class
 */

#include <catch2/catch.hpp>

#include <vtkNew.h>
#include <vtkSphereSource.h>
#include <vtkTriangleFilter.h>

#include "Surface.h"

using namespace hgve;

SCENARIO("Basic operations on Surface", "[surface]") {
  // Generate a sphere
  GIVEN("A sphere") {
    vtkNew<vtkSphereSource> sphere;
    sphere->SetRadius(2);
    sphere->SetThetaResolution(8);
    sphere->SetPhiResolution(8);
    sphere->Update();

    vtkNew<vtkTriangleFilter> triFilter;
    triFilter->SetInputData(sphere->GetOutput());
    triFilter->Update();

    Surface surface(triFilter->GetOutput());
    surface.Clean(1e-4);

    WHEN("Serializing the surface to disk without Geometrical Data") {
      THEN("I can save it to VTK format") { surface.Write("surface.vtp"); }

      AND_THEN("I can save it to ASCII format") {
        surface.Write("surface.ascii");
      }

      AND_THEN("An exception is thrown when serializing to unknown format") {
        REQUIRE_THROWS(surface.Write("surface.nonexistent"));
      }
    }

    WHEN("Serializing the surface to disk with Geometrical Data") {
      surface.ComputeCurvatures();

      THEN("I can save it to VTK format") { surface.Write("surface.vtp"); }

      AND_THEN("I can save it to ASCII format") {
        surface.Write("surface.ascii");
      }
    }
  }
}
