#include <catch2/catch.hpp>

#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkTriangle.h>

#include "Surface.h"

#include "test_config.h"

using namespace hgve;

SCENARIO("Testing the Averaging method for a Surface", "[average]") {
  GIVEN("A simple planar square surface") {
    // Points of the roof-shaped patch
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->InsertNextPoint(0, 0, 0);    // #0
    points->InsertNextPoint(0, 1, 1);    // #1
    points->InsertNextPoint(0, -1, 1);   // #2
    points->InsertNextPoint(0, -1, -1);  // #3
    points->InsertNextPoint(0, 1, -1);   // #4

    // Triangles
    vtkSmartPointer<vtkTriangle> t0 = vtkSmartPointer<vtkTriangle>::New();
    t0->GetPointIds()->SetId(0, 0);
    t0->GetPointIds()->SetId(1, 1);
    t0->GetPointIds()->SetId(2, 2);
    vtkSmartPointer<vtkTriangle> t1 = vtkSmartPointer<vtkTriangle>::New();
    t1->GetPointIds()->SetId(0, 0);
    t1->GetPointIds()->SetId(1, 2);
    t1->GetPointIds()->SetId(2, 3);
    vtkSmartPointer<vtkTriangle> t2 = vtkSmartPointer<vtkTriangle>::New();
    t2->GetPointIds()->SetId(0, 0);
    t2->GetPointIds()->SetId(1, 3);
    t2->GetPointIds()->SetId(2, 4);
    vtkSmartPointer<vtkTriangle> t3 = vtkSmartPointer<vtkTriangle>::New();
    t3->GetPointIds()->SetId(0, 0);
    t3->GetPointIds()->SetId(1, 4);
    t3->GetPointIds()->SetId(2, 1);

    vtkSmartPointer<vtkCellArray> triangles =
        vtkSmartPointer<vtkCellArray>::New();
    triangles->InsertNextCell(t0);
    triangles->InsertNextCell(t1);
    triangles->InsertNextCell(t2);
    triangles->InsertNextCell(t3);

    vtkSmartPointer<vtkPolyData> patch = vtkSmartPointer<vtkPolyData>::New();
    patch->SetPoints(points);
    patch->SetPolys(triangles);

    Surface surface(patch);
    surface.ComputeCurvatures();
    surface.Write("pre.vtp");
    // double preA0 = surface.GetCurvArray().Get(0).A();

    WHEN("We average the points") {
      surface.Average(3);
      surface.Write("average.vtp");

      THEN(
          "The value of the curvature at the center point is equal "
          "to the one at the corners") {
        double G0 = surface.GetCurvArray().Get(0).G();
        double G1 = surface.GetCurvArray().Get(1).G();
        double G2 = surface.GetCurvArray().Get(2).G();
        double G3 = surface.GetCurvArray().Get(3).G();

        REQUIRE(G0 == Approx(G1).epsilon(epsilon));
        REQUIRE(G1 == Approx(G2).epsilon(epsilon));
        REQUIRE(G2 == Approx(G3).epsilon(epsilon));
        REQUIRE(G3 == Approx(G0).epsilon(epsilon));
      }

      // AND_THEN("The area is not changed by the averaging process"){
      //    double A0 = surface.GetCurvArray().Get(0).A();

      //    REQUIRE(preA0 == A0);

      //}
    }
  }
}
