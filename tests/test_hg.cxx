/** This is the main file for catch tests
 */

#define APPROX_MARGIN 1E-6

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
