#include <catch2/catch.hpp>
#include <cmath>

#include <vtkActor.h>
#include <vtkMapper.h>
#include <vtkMassProperties.h>
#include <vtkNew.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTriangleFilter.h>

#include "Edge.h"
#include "ItemHash.h"
#include "Surface.h"
#include "SurfaceWriterFactory.h"
#include "types.h"

#include "test_config.h"

using namespace hgve;

SCENARIO("Differential geometry parameters for a sphere",
         "[sphere][!mayfail]") {
  double sphereRadius = GENERATE(2.0, 0.1, 0.01);
  double sphereArea = 4.0 * M_PI * sphereRadius * sphereRadius;

  GIVEN("A sphere of defined radius (" + std::to_string(sphereRadius) + ")") {
    vtkNew<vtkSphereSource> sphere;
    // I initialize always a R=1 sphere, but I use the spacing to scale it
    // down
    sphere->SetRadius(sphereRadius);

    WHEN("The sphere has a coarse triangulation") {
      sphere->SetPhiResolution(coarseResolution);
      sphere->SetThetaResolution(coarseResolution);
      sphere->Update();

      // Transform the sphere into a
      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(sphere->GetOutput());
      triFilter->Update();

      // Triangulated version of the sphere
      vtkSmartPointer<vtkPolyData> triSphere = triFilter->GetOutput();

      // Get total number of points
      IdType nPoints = triSphere->GetNumberOfPoints();

      // Get total number of cells
      IdType nCells = triSphere->GetNumberOfCells();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());

      // Clean
      surface.Clean(sphereRadius / coarseResolution / coarseResolution);
      // surface.Sanitize();

      REQUIRE(surface.GetNumberOfCells() == nCells);
      REQUIRE(surface.GetNumberOfPoints() == nPoints);

      WHEN("Iterating over the surface points") {
        THEN("Each point has norm equal to the sphere radius ") {
          for (auto p : surface.IterateOver<Point>()) {
            REQUIRE(p.Norm() == Approx(sphereRadius));
          }
        }
      }

      WHEN("Iterating over the surface cells") {
        double A = 0;
        for (auto c : surface.IterateOver<Cell>()) {
          A += c.Area();
        }

        THEN("The sum of cell areas is equal to surface area") {
          vtkSmartPointer<vtkMassProperties> mp =
              vtkSmartPointer<vtkMassProperties>::New();
          mp->SetInputData(triSphere);
          REQUIRE(A == Approx(mp->GetSurfaceArea()));
        }
      }

      WHEN("Iterating over the 1-ring regions of each point") {
        Set<Point> allPoints;
        double A = 0;
        double A2 = 0;
        THEN("The number of 1-ring edges is equal to the points") {
          for (auto p : surface.IterateOver<Point>()) {
            auto oneRingPoints = p.OneRingPoints();
            auto oneRingEdges = p.OneRingEdges();
            A += p.OneRingArea();
            A2 += p.OneRingProperties().A();

            allPoints.insert(oneRingPoints.begin(), oneRingPoints.end());
            REQUIRE(oneRingEdges.size() == oneRingPoints.size());
          }

          THEN(
              "All the 1-ring points are the total amount of "
              "points") {
            REQUIRE(static_cast<int>(allPoints.size()) ==
                    surface.GetNumberOfPoints());
          }

          THEN("The sum of the 1-ring areas is the surface area") {
            REQUIRE(A == Approx(surface.Area()));
          }

          THEN(
              "The sum of the 1-ring areas appendend to each point "
              "is the surface area") {
            REQUIRE(A2 == Approx(surface.Area()));
          }
        }
      }

      WHEN("Iterating over the 1-ring edges") {
        double A = 0;
        for (auto p : surface.IterateOver<Point>()) {
          auto oneRingEdges = p.OneRingEdges();

          for (auto e : oneRingEdges) {
            auto edgeCells = e.EdgeCells();
            A += edgeCells.first.Area();
            A += edgeCells.second.Area();
          }
        }

        THEN(
            "The area computed summing all the edge cell pairs is "
            "equal to six times the area of the triangulated "
            "surface") {
          REQUIRE(A == Approx(6 * surface.Area()));
        }
      }

      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          AND_THEN("I'm able to save the surface in a file") {
            surface.Write("sphere-coarse.vtp");
          }
        }
      }
    }  // WHEN SPHERE
    WHEN("The sphere has a fine enough triangulation") {
      sphere->SetPhiResolution(fineResolution);
      sphere->SetThetaResolution(fineResolution);
      sphere->Update();

      // Transform the sphere into a
      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(sphere->GetOutput());
      triFilter->Update();

      vtkSmartPointer<vtkPolyData> triSphere = triFilter->GetOutput();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());
      surface.Clean(sphereRadius / fineResolution / fineResolution);

      WHEN("Iterating over the surface cells") {
        double A = 0;
        for (auto c : surface.IterateOver<Cell>()) {
          A += c.Area();
        }

        THEN("The sum of cell areas is equal to surface area") {
          vtkSmartPointer<vtkMassProperties> mp =
              vtkSmartPointer<vtkMassProperties>::New();
          mp->SetInputData(triSphere);
          REQUIRE(A == Approx(mp->GetSurfaceArea()));
        }
      }

      THEN(
          "The value of the area computed is equal to the analytical "
          "one") {
        REQUIRE(surface.Area() == Approx(sphereArea).margin(approxMargin));
      }
      AND_THEN(
          "The value of the mean curvature at each point is equal "
          "to 1/r") {
        for (auto p : surface.IterateOver<Point>()) {
          REQUIRE(p.OneRingProperties().H() ==
                  Approx(1 / sphereRadius).margin(approxMargin));
        }
      }

      AND_THEN(
          "The value of the gauss curvature at each point is equal "
          " to 1/(r*r)") {
        for (auto p : surface.IterateOver<Point>()) {
          REQUIRE(p.OneRingProperties().G() ==
                  Approx(1 / sphereRadius / sphereRadius).margin(approxMargin));
        }
      }

      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          surface.Write("sphere-fine.vtp");
        }
      }

      WHEN("Averaging in a neighbourhood wide twice the radius") {
        surface.ComputeCurvatures();
        auto previousCurvs = surface.GetCurvArray();
        surface.Average(0.01 * sphereRadius);
        auto n = surface.GetNumberOfPoints();

        THEN(
            "The value stored has not changed since the curvature of "
            " a sphere is constant ") {
          surface.Write("sphere-averaged.vtp");
          auto newCurvs = surface.GetCurvArray();
          IdType inliers = 0;

          for (auto i = 0; i < n; i++) {
            for (auto field : CurvaturesData::m_names) {
              // We evaluate lazily since we can have
              // few points that have bad values
              auto err = previousCurvs.Get(i)[field] - newCurvs.Get(i)[field];
              if (err < 1E-6) inliers++;
            }
          }

          // At least 99% of the points are good
          REQUIRE(inliers / (CurvaturesData::m_names.size()) > 0.99);
        }
      }
    }
  }  // GIVEN
}  // SCENARIO
