#include <catch2/catch.hpp>

#include "BlockMerger.h"
#include "BlockMirror.h"
#include "Volume.h"

using namespace hgve;

static void AllocateBlock(vtkImageData *block) {
  // Allocate in the block as value the coordinates of the points
  int *dims = block->GetDimensions();
  for (int i = 0; i < dims[0]; i++) {
    for (int j = 0; j < dims[1]; j++) {
      for (int k = 0; k < dims[2]; k++) {
        block->SetScalarComponentFromDouble(i, j, k, 0, i * j * k);
      }
    }
  }
}

SCENARIO("Mirror a block over along an axis", "[mirror]") {
  GIVEN("An vtkImageData object") {
    vtkSmartPointer<vtkImageData> block = vtkSmartPointer<vtkImageData>::New();
    block->SetDimensions(4, 4, 4);
    block->SetOrigin(0, 0, 0);
    block->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block);

    WHEN("We mirror the block along the x-axis") {
      Direction axis = Direction::X;
      Orientation orientation = Orientation::POS;
      BlockMirror mirror(block, axis, orientation);
      mirror.Mirror();

      vtkSmartPointer<vtkImageData> flipBlock = mirror.GetOutput();

      THEN(
          "The bounds of the mirrored block starts at the outer bounds of the "
          "input block") {
        double *originBounds = block->GetBounds();
        double *mirrorBounds = flipBlock->GetBounds();

        REQUIRE(mirrorBounds[0] == originBounds[1]);
      }

      WHEN("We merge the original block and the flipped one") {
        std::vector<GridBlock> blocks;
        blocks.push_back(GridBlock(block));
        blocks.push_back(GridBlock(flipBlock));
        BlockMerger merger(blocks);
        merger.SetBoundariesOverlapping(false);
        merger.Merge();

        THEN("The number of elements along the mirrored axis is doubled") {
          REQUIRE(merger.GetOutput()->GetDimensions()[0] ==
                  2 * block->GetDimensions()[0]);
        }
      }
    }

    WHEN("We mirror the block along the x-axis in negative orientation") {
      Direction axis = Direction::X;
      Orientation orientation = Orientation::NEG;
      BlockMirror mirror(block, axis, orientation);
      mirror.Mirror();

      vtkSmartPointer<vtkImageData> flipBlock = mirror.GetOutput();

      THEN(
          "The bounds of the mirrored block starts at the outer bounds of the "
          "input block") {
        double *originBounds = block->GetBounds();
        double *mirrorBounds = flipBlock->GetBounds();

        REQUIRE(mirrorBounds[3] == originBounds[3]);
      }

      WHEN("We merge the original block and the flipped one") {
        std::vector<GridBlock> blocks;
        blocks.push_back(GridBlock(block));
        blocks.push_back(GridBlock(flipBlock));
        BlockMerger merger(blocks);
        merger.SetBoundariesOverlapping(false);
        merger.Merge();

        Volume volume(merger.GetOutput());
        volume.Write("prova.vti");

        THEN("The number of elements along the mirrored axis is doubled") {
          REQUIRE(merger.GetOutput()->GetDimensions()[0] ==
                  2 * block->GetDimensions()[0]);
        }
        AND_THEN("The bounds of the mirrored block along x are negative") {
          REQUIRE(merger.GetOutput()->GetBounds()[0] == -block->GetBounds()[1]);
        }
      }
    }

    WHEN("We mirror the block along the y-axis") {
      Direction axis = Direction::Y;
      Orientation orientation = Orientation::POS;
      BlockMirror mirror(block, axis, orientation);
      mirror.Mirror();

      vtkSmartPointer<vtkImageData> flipBlock = mirror.GetOutput();

      THEN(
          "The bounds of the mirrored block starts at the outer bounds of the "
          "input block") {
        double *originBounds = block->GetBounds();
        double *mirrorBounds = flipBlock->GetBounds();

        REQUIRE(mirrorBounds[2] == originBounds[3]);
      }

      WHEN("We merge the original block and the flipped one") {
        std::vector<GridBlock> blocks;
        blocks.push_back(GridBlock(block));
        blocks.push_back(GridBlock(flipBlock));
        BlockMerger merger(blocks);
        merger.SetBoundariesOverlapping(false);
        merger.Merge();

        THEN("The number of elements along the mirrored axis is doubled") {
          REQUIRE(merger.GetOutput()->GetDimensions()[1] ==
                  2 * block->GetDimensions()[1]);
        }
      }
    }

    WHEN("We mirror the block along the x- and y-axis") {
      Direction axis = Direction::X;
      Orientation orientation = Orientation::POS;
      BlockMirror mirror(block, axis, orientation);
      mirror.Mirror();

      std::vector<GridBlock> blocks;
      blocks.push_back(GridBlock(block));
      blocks.push_back(GridBlock(mirror.GetOutput()));

      BlockMerger merger(blocks);
      merger.SetBoundariesOverlapping(false);
      merger.Merge();

      axis = Direction::Y;
      orientation = Orientation::POS;
      mirror = BlockMirror(merger.GetOutput(), axis, orientation);
      mirror.Mirror();

      vtkSmartPointer<vtkImageData> flipBlock = mirror.GetOutput();

      WHEN("We merge the original block and the flipped one") {
        blocks = {{GridBlock(merger.GetOutput())},
                  {GridBlock(mirror.GetOutput())}};
        merger = BlockMerger(blocks);
        merger.SetBoundariesOverlapping(false);
        merger.Merge();

        THEN("The number of elements along the mirrored axes are doubled") {
          REQUIRE(merger.GetOutput()->GetDimensions()[0] ==
                  2 * block->GetDimensions()[0]);
          REQUIRE(merger.GetOutput()->GetDimensions()[1] ==
                  2 * block->GetDimensions()[1]);
          Volume volume(merger.GetOutput());
          volume.Write("mirror.vti");
        }
      }
    }
  }
}
