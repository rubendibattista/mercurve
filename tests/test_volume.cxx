/**
 * @brief Testing Volume class
 */

#include <catch2/catch.hpp>

#include "Volume.h"

using namespace hgve;

static const int nPoints = 10;

SCENARIO("Basic operations on Volume", "[volume]") {
  GIVEN("A volume object initialized with the base constructor") {
    Volume volume(nPoints, nPoints, nPoints, 1, 1, 1);

    THEN("The number of points is correctly stored") {
      REQUIRE(volume.GetNumberOfPoints() == nPoints * nPoints * nPoints);
    }
  }

  GIVEN("A volume object initialized with the vector constructor") {
    Volume volume(nPoints, nPoints, nPoints, SimpleVector(1, 1, 1));

    THEN("The number of points is correctly stored") {
      REQUIRE(volume.GetNumberOfPoints() == nPoints * nPoints * nPoints);
    }

    AND_THEN("The spacing is correctly handled") {
      auto spacing = volume.GetSpacing();
      REQUIRE(std::all_of(
          spacing.begin(), spacing.end(),
          [spacing](double elem) { return elem == spacing[0]; })  // all_of
      );                                                          // REQUIRE

      REQUIRE(spacing[0] == Approx(0.1));
    }
  }

  GIVEN("A volume object initialized with the array constructor") {
    Volume volume(ThreeArray<IdType>{nPoints, nPoints, nPoints},
                  ThreeArray<double>{1, 1, 1});

    THEN("The number of points is correctly stored") {
      REQUIRE(volume.GetNumberOfPoints() == nPoints * nPoints * nPoints);
    }

    AND_THEN("The spacing is correctly handled") {
      auto spacing = volume.GetSpacing();
      REQUIRE(std::all_of(
          spacing.begin(), spacing.end(),
          [spacing](double elem) { return elem == spacing[0]; })  // all_of
      );                                                          // REQUIRE

      REQUIRE(spacing[0] == Approx(1));
    }
  }
}
