/**
 * @brief Testing the computation of curvatures on a torus
 */
#include <catch2/catch.hpp>
#include <cmath>

#include <vtkCleanPolyData.h>
#include <vtkMapper.h>
#include <vtkNew.h>
#include <vtkSuperquadricSource.h>
#include <vtkTriangleFilter.h>

#include "Edge.h"
#include "ItemHash.h"
#include "Surface.h"
#include "types.h"

#include "test_config.h"

using namespace hgve;

SCENARIO("Differential geometry parameters for a torus", "[torus]") {
  GIVEN("A torus") {
    vtkSmartPointer<vtkSuperquadricSource> torus =
        vtkSmartPointer<vtkSuperquadricSource>::New();
    torus->SetCenter(0.0, 0.0, 0.0);
    torus->SetScale(1.0, 1.0, 1.0);
    torus->SetThetaRoundness(1);
    torus->SetThickness(0.5);
    torus->SetSize(1);
    torus->SetToroidal(true);

    WHEN("The torus has a coarse triangulation") {
      torus->SetPhiResolution(coarseResolution);
      torus->SetThetaResolution(coarseResolution);
      torus->Update();

      // Transform the torus into a PolyData
      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(torus->GetOutput());
      triFilter->Update();

      // Triangulated version of the torus
      vtkSmartPointer<vtkPolyData> triSphere = triFilter->GetOutput();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());
      surface.Clean(1E-5);

      // Clean removing cells not having 3 points

      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          AND_THEN("I'm able to save the surface in a file") {
            surface.Write("torus-coarse.vtp");
          }
          AND_THEN(
              "The integral of the Gauss curvature over the object "
              "(Gauss-Bonnet) is equal to zero") {
            double GA = 0;
            for (auto p : surface.IterateOver<Point>()) {
              auto prop = p.OneRingProperties();
              GA += prop.G() * prop.A();
            }

            // I need to add margin since checking for zero
            // matching with floats is hard due to float numbers
            // denormalized bit representation
            // https://github.com/catchorg/Catch2/issues/1096/
            REQUIRE(std::abs(GA) == Approx(0).margin(1E-12));
          }
        }
      }
    }  // WHEN SPHERE
    WHEN("The torus has a fine enough triangulation") {
      torus->SetPhiResolution(0.2 * fineResolution);
      torus->SetThetaResolution(0.2 * fineResolution);
      torus->Update();
      torus->Update();

      // Transform the torus into a
      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(torus->GetOutput());
      triFilter->Update();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());
      surface.Clean(1E-5);
      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          AND_THEN("I'm able to save the surface in a file") {
            surface.Write("torus-fine.vtp");
          }

          AND_THEN(
              "The integral of the Gauss curvature over the object "
              "(Gauss-Bonnet) is equal to zero") {
            double GA = 0;
            for (auto p : surface.IterateOver<Point>()) {
              auto prop = p.OneRingProperties();
              GA += prop.G() * prop.A();
            }

            // I need to add margin since checking for zero
            // matching with floats is hard due to float numbers
            // denormalized bit representation
            // https://github.com/catchorg/Catch2/issues/1096/
            REQUIRE(std::abs(GA) == Approx(0).margin(1E-12));
          }
        }
      }
    }
  }  // GIVEN
}  // SCENARIO
